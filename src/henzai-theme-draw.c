/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8; fill-column: 160 -*- */
/*
 * $Id$
 *
 * Copyright (C) 1999 Dwight Engen <dengen40@yahoo.com>
 *
 * The sources to the GTKstep theme by Ullrich Hafner <hafner@bigfoot.de>
 * and the ThinIce theme by Tim & Tomas <timg@means.net> &
 * <stric@ing.umu.se> were both invaluable and borrowed from.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "henzai-theme.h"
#include "henzai-theme-style.h"
#include "henzai-theme-rc-style.h"
#include <math.h>

#define DETAIL(xx) ((detail) && (!strcmp(xx, detail)))
// #define DEBUG	// if you want lots of printf goop comming out

/*
 * The XFree86 Mach64 server seems not to draw lines exactly like you tell
 * it to, so if you define this, we'll try to draw at least arrows right
 * with this server.
 */
#define BROKEN_MACH64

#define SET_GC_OUT(rc_style)				\
    if (rc_style->black_and_white)	\
    {							\
	gc1 = style->white_gc;				\
	gc2 = style->black_gc;				\
    }							\
    else						\
    {							\
	gc1 = style->light_gc[state_type];		\
	gc2 = style->dark_gc[state_type];		\
    }

#define SET_GC_IN(rc_style)				\
    if (rc_style->black_and_white)	\
    {							\
	gc1 = style->black_gc;				\
	gc2 = style->white_gc;				\
    }							\
    else						\
    {							\
	gc1 = style->dark_gc[state_type];		\
	gc2 = style->light_gc[state_type];		\
    }



static GtkStyleClass *parent_class;

GType henzai_type_style = 0;



static void	draw_hline(GtkStyle * style,
			   GdkWindow * window,
			   GtkStateType state_type,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   gint x1,
			   gint x2,
			   gint y);
static void	draw_vline(GtkStyle * style,
			   GdkWindow * window,
			   GtkStateType state_type,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   gint y1,
			   gint y2,
			   gint x);
static void	draw_shadow(GtkStyle * style,
			    GdkWindow * window,
			    GtkStateType state_type,
			    GtkShadowType shadow_type,
			    GdkRectangle * area,
			    GtkWidget * widget,
			    const gchar *detail,
			    gint x,
			    gint y,
			    gint width,
			    gint height);
static void	draw_polygon(GtkStyle * style,
			     GdkWindow * window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle * area,
			     GtkWidget * widget,
			     const gchar *detail,
			     GdkPoint * point,
			     gint npoints,
			     gint fill);
static void	draw_arrow(GtkStyle * style,
			   GdkWindow * window,
			   GtkStateType state_type,
			   GtkShadowType shadow_type,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   GtkArrowType arrow_type,
			   gint fill,
			   gint x,
			   gint y,
			   gint width,
			   gint height);
static void	draw_diamond(GtkStyle * style,
			     GdkWindow * window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle * area,
			     GtkWidget * widget,
			     const gchar *detail,
			     gint x,
			     gint y,
			     gint width,
			     gint height);
static void	draw_oval(GtkStyle * style,
			  GdkWindow * window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle * area,
			  GtkWidget * widget,
			  const gchar *detail,
			  gint x,
			  gint y,
			  gint width,
			  gint height);
static void	draw_string(GtkStyle * style,
			    GdkWindow * window,
			    GtkStateType state_type,
			    GdkRectangle * area,
			    GtkWidget * widget,
			    const gchar *detail,
			    gint x,
			    gint y,
			    const gchar * string);
static void	draw_box(GtkStyle * style,
			 GdkWindow * window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle * area,
			 GtkWidget * widget,
			 const gchar *detail,
			 gint x,
			 gint y,
			 gint width,
			 gint height);
static void	draw_flat_box(GtkStyle * style,
			      GdkWindow * window,
			      GtkStateType state_type,
			      GtkShadowType shadow_type,
			      GdkRectangle * area,
			      GtkWidget * widget,
			      const gchar *detail,
			      gint x,
			      gint y,
			      gint width,
			      gint height);
static void	draw_check(GtkStyle * style,
			   GdkWindow * window,
			   GtkStateType state_type,
			   GtkShadowType shadow_type,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   gint x,
			   gint y,
			   gint width,
			   gint height);
static void	draw_option(GtkStyle * style,
			    GdkWindow * window,
			    GtkStateType state_type,
			    GtkShadowType shadow_type,
			    GdkRectangle * area,
			    GtkWidget * widget,
			    const gchar *detail,
			    gint x,
			    gint y,
			    gint width,
			    gint height);
static void	draw_cross(GtkStyle * style,
			   GdkWindow * window,
			   GtkStateType state_type,
			   GtkShadowType shadow_type,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   gint x,
			   gint y,
			   gint width,
			   gint height);
static void	draw_ramp(GtkStyle * style,
			  GdkWindow * window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle * area,
			  GtkWidget * widget,
			  const gchar *detail,
			  GtkArrowType arrow_type,
			  gint x,
			  gint y,
			  gint width,
			  gint height);
static void	draw_tab(GtkStyle * style,
			 GdkWindow * window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle * area,
			 GtkWidget * widget,
			 const gchar *detail,
			 gint x,
			 gint y,
			 gint width,
			 gint height);
static void	draw_shadow_gap(GtkStyle * style,
				GdkWindow * window,
				GtkStateType state_type,
				GtkShadowType shadow_type,
				GdkRectangle * area,
				GtkWidget * widget,
				const gchar *detail,
				gint x,
				gint y,
				gint width,
				gint height,
				GtkPositionType gap_side,
				gint gap_x,
				gint gap_width);
static void	draw_box_gap(GtkStyle * style,
			     GdkWindow * window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle * area,
			     GtkWidget * widget,
			     const gchar *detail,
			     gint x,
			     gint y,
			     gint width,
			     gint height,
			     GtkPositionType gap_side,
			     gint gap_x,
			     gint gap_width);
static void	draw_extension(GtkStyle * style,
			       GdkWindow * window,
			       GtkStateType state_type,
			       GtkShadowType shadow_type,
			       GdkRectangle * area,
			       GtkWidget * widget,
			       const gchar *detail,
			       gint x,
			       gint y,
			       gint width,
			       gint height,
			       GtkPositionType gap_side);
static void	draw_focus(GtkStyle * style,
			   GdkWindow * window,
			   GdkRectangle * area,
			   GtkWidget * widget,
			   const gchar *detail,
			   gint x,
			   gint y,
			   gint width,
			   gint height);
static void	draw_slider(GtkStyle * style,
			    GdkWindow * window,
			    GtkStateType state_type,
			    GtkShadowType shadow_type,
			    GdkRectangle * area,
			    GtkWidget * widget,
			    const gchar *detail,
			    gint x,
			    gint y,
			    gint width,
			    gint height,
			    GtkOrientation orientation);
static void	draw_handle(GtkStyle * style,
			    GdkWindow * window,
			    GtkStateType state_type,
			    GtkShadowType shadow_type,
			    GdkRectangle * area,
			    GtkWidget * widget,
			    const gchar *detail,
			    gint x,
			    gint y,
			    gint width,
			    gint height,
			    GtkOrientation orientation);
static void	draw_dimple(GtkStyle *style,
			    GdkWindow *window,
			    GtkStateType state_type,
			    int width,
			    int height,
			    int x,
			    int y,
			    gboolean horizontal);
static void	henzai_tab(GtkStyle * style,
			  GdkWindow * window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle * area,
			  GtkWidget * widget,
			  const gchar *detail,
			  gint x,
			  gint y,
			  gint width,
			  gint height);


static void
draw_hline(GtkStyle * style,
	   GdkWindow * window,
	   GtkStateType state_type,
	   GdkRectangle * area,
	   GtkWidget * widget, 
	   const gchar *detail, 
	   gint x1, gint x2, gint y)
{
	gint thickness_light;
	gint thickness_dark;
	gint i;
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	HenzaiRcStyle *henzai_rc_style;


	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_hline", detail);
#endif
	thickness_light = style->ythickness / 2;
	thickness_dark = style->ythickness - thickness_light;
	
	
	
	if (DETAIL("menuitem"))
		y++;
	
	SET_GC_OUT(henzai_rc_style);
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
		}
	for (i = 0; i < thickness_dark; i++)
		{
			gdk_draw_line(window, gc2, x1, y + i, x2 - i - 1, y + i);
		}
	
	y += thickness_dark;
	for (i = 0; i < thickness_light; i++)
		{
			gdk_draw_line(window, gc2, x1, y + i, x1 + thickness_light - i - 1,
				      y + i);
			gdk_draw_line(window, gc1, x1 + thickness_light - i - 1, y + i, x2,
				      y + i);
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
		}
}

static void
draw_vline(GtkStyle * style,
	   GdkWindow * window,
	   GtkStateType state_type,
	   GdkRectangle * area,
	   GtkWidget * widget, const gchar *detail, gint y1, gint y2, gint x)
{
	gint thickness_light;
	gint thickness_dark;
	gint i;
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_vline", detail);
#endif
	
	thickness_light = style->xthickness / 2;
	thickness_dark = style->xthickness - thickness_light;
	
	SET_GC_OUT(henzai_rc_style);
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
		}
	for (i = 0; i < thickness_dark; i++)
		{
			gdk_draw_line(window, gc1, x + i, y2 - i - 1, x + i, y2);
			gdk_draw_line(window, gc2, x + i, y1, x + i, y2 - i - 1);
		}
	
	x += thickness_dark;
	for (i = 0; i < thickness_light; i++)
		{
			gdk_draw_line(window, gc2, x + i, y1, x + i, y1 + thickness_light - i);
			gdk_draw_line(window, gc1, x + i, y1 + thickness_light - i, x + i, y2);
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
		}
}

static void
draw_shadow(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle * area,
	    GtkWidget * widget,
	    const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_shadow", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	switch (shadow_type)
		{
		case GTK_SHADOW_NONE:
			return;
		case GTK_SHADOW_IN:
		case GTK_SHADOW_ETCHED_IN:
			SET_GC_OUT(henzai_rc_style);
			break;
		case GTK_SHADOW_OUT:
		case GTK_SHADOW_ETCHED_OUT:
			SET_GC_IN(henzai_rc_style);
			break;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
		}
	
	if (DETAIL("entry"))
		gdk_draw_rectangle(window, style->bg_gc[GTK_STATE_ACTIVE], TRUE,
				   x, y, width - 1, height - 1);
	
	switch (shadow_type)
		{
		case GTK_SHADOW_NONE:
			break;
			
		case GTK_SHADOW_ETCHED_IN:
		case GTK_SHADOW_ETCHED_OUT:
			// normal darks
			gdk_draw_line(window, gc1, x, y + height - 1, x + width - 1,
				      y + height - 1);
			gdk_draw_line(window, gc1, x + width - 1, y, x + width - 1,
				      y + height - 1);
			gdk_draw_line(window, gc2, x, y, x + width - 2, y);
			gdk_draw_line(window, gc2, x, y, x, y + height - 2);
			
			// normal lights
			gdk_draw_line(window, gc1, x + 1, y + 1, x + width - 2, y + 1);
			gdk_draw_line(window, gc1, x + 1, y + 1, x + 1, y + height - 2);
			gdk_draw_line(window, gc2, x + 1, y + height - 2, x + width - 2,
				      y + height - 2);
			gdk_draw_line(window, gc2, x + width - 2, y + 1, x + width - 2,
				      y + height - 2);
			break;
			
		case GTK_SHADOW_IN:
		case GTK_SHADOW_OUT:
			gdk_draw_line(window, gc2, x, y, x + width - 1, y);
			gdk_draw_line(window, gc2, x, y, x, y + height - 1);
			
			gdk_draw_line(window, gc1, x, y + height - 1, x + width - 1,
				      y + height - 1);
			gdk_draw_line(window, gc1, x + width - 1, y, x + width - 1,
				      y + height - 1);
			break;
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
		}
}

static void
draw_polygon(GtkStyle * style,
	     GdkWindow * window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle * area,
	     GtkWidget * widget,
	     const gchar *detail, GdkPoint * points, gint npoints, gint fill)
{
#ifndef M_PI
#define M_PI    3.14159265358979323846
#endif /* M_PI */
#ifndef M_PI_4
#define M_PI_4  0.78539816339744830962
#endif /* M_PI_4 */
	
	static const gdouble pi_over_4 = M_PI_4;
	static const gdouble pi_3_over_4 = M_PI_4 * 3;
	
	GdkGC *gc1;
	GdkGC *gc2;
	GdkGC *gc3;
	GdkGC *gc4;
	gdouble angle;
	gint xadjust;
	gint yadjust;
	gint i;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
	g_return_if_fail(points != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_polygon", detail);
#endif
	
	switch (shadow_type)
		{
		case GTK_SHADOW_IN:
			gc1 = style->light_gc[state_type];
			gc2 = style->dark_gc[state_type];
			gc3 = style->light_gc[state_type];
			gc4 = style->dark_gc[state_type];
			break;
		case GTK_SHADOW_ETCHED_IN:
			gc1 = style->light_gc[state_type];
			gc2 = style->dark_gc[state_type];
			gc3 = style->dark_gc[state_type];
			gc4 = style->light_gc[state_type];
			break;
		case GTK_SHADOW_OUT:
			gc1 = style->dark_gc[state_type];
			gc2 = style->light_gc[state_type];
			gc3 = style->dark_gc[state_type];
			gc4 = style->light_gc[state_type];
			break;
		case GTK_SHADOW_ETCHED_OUT:
			gc1 = style->dark_gc[state_type];
			gc2 = style->light_gc[state_type];
			gc3 = style->light_gc[state_type];
			gc4 = style->dark_gc[state_type];
			break;
		default:
			return;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
			gdk_gc_set_clip_rectangle(gc3, area);
			gdk_gc_set_clip_rectangle(gc4, area);
		}
	
	if (fill)
		gdk_draw_polygon(window, style->bg_gc[state_type], TRUE, points,
				 npoints);
	
	npoints--;
	
	for (i = 0; i < npoints; i++)
		{
			if ((points[i].x == points[i + 1].x) &&
			    (points[i].y == points[i + 1].y))
				{
					angle = 0;
				}
			else
				{
					angle = atan2(points[i + 1].y - points[i].y,
						      points[i + 1].x - points[i].x);
				}
			
			if ((angle > -pi_3_over_4) && (angle < pi_over_4))
				{
					if (angle > -pi_over_4)
						{
							xadjust = 0;
							yadjust = 1;
						}
					else
						{
							xadjust = 1;
							yadjust = 0;
						}
					
					gdk_draw_line(window, gc1,
						      points[i].x - xadjust, points[i].y - yadjust,
						      points[i + 1].x - xadjust, points[i + 1].y - yadjust);
					gdk_draw_line(window, gc3,
						      points[i].x, points[i].y,
						      points[i + 1].x, points[i + 1].y);
				}
			else
				{
					if ((angle < -pi_3_over_4) || (angle > pi_3_over_4))
						{
							xadjust = 0;
							yadjust = 1;
						}
					else
						{
							xadjust = 1;
							yadjust = 0;
						}
					
					gdk_draw_line(window, gc4,
						      points[i].x + xadjust, points[i].y + yadjust,
						      points[i + 1].x + xadjust, points[i + 1].y + yadjust);
					gdk_draw_line(window, gc2,
						      points[i].x, points[i].y,
						      points[i + 1].x, points[i + 1].y);
				}
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
			gdk_gc_set_clip_rectangle(gc3, NULL);
			gdk_gc_set_clip_rectangle(gc4, NULL);
		}
}

static void
draw_dimple(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    int width,
	    int height,
	    int x,
	    int y,
	    gboolean horizontal)
{
	int centerx;
	int centery;
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	HenzaiRcStyle *henzai_rc_style;

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

	centerx = (width - 1) / 2 + x;
	centery = (height - 1) / 2 + y;
	
	SET_GC_OUT(henzai_rc_style);
	if (horizontal)
		{
			gdk_draw_point(window, gc2, centerx, centery);
			gdk_draw_point(window, gc1, centerx + 1, centery);
		}
	else
		{
			gdk_draw_point(window, gc2, centerx, centery);
			gdk_draw_point(window, gc1, centerx, centery + 1);
		}
}


static void
draw_arrow(GtkStyle * style,
	   GdkWindow * window,
	   GtkStateType state_type,
	   GtkShadowType shadow_type,
	   GdkRectangle * area,
	   GtkWidget * widget,
	   const gchar *detail,
	   GtkArrowType arrow_type,
	   gint fill, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1;
	GdkGC *gc2;
	gint as;			// arrow size
	gint xl, xm, xr;		// left, mid, right
	gint yt, ym, yb;		// top,  mid, bottom
	GdkPoint points[4];
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_arrow", detail);
#endif
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	if (DETAIL("menuitem"))
		{
			/* draw indented triangle */
			if (henzai_rc_style->small_menu_arrows)
				{
					points[0].x = x;
					points[0].y = y + 1;
					points[1].x = x + (height - 2) / 2;
					points[1].y = y + height / 2;
					points[2].x = x;
					points[2].y = y + height - 1;
				}
			else
				{
					points[0].x = x;
					points[0].y = y;
					points[1].x = x + (height) / 2;
					points[1].y = y + height / 2;
					points[2].x = x;
					points[2].y = y + height;
				}
			gdk_draw_polygon(window, style->bg_gc[GTK_STATE_ACTIVE], TRUE, points,
					 3);
			gdk_draw_line(window, style->black_gc, points[0].x, points[0].y,
				      points[2].x, points[2].y);
			gdk_draw_line(window, style->black_gc, points[0].x, points[0].y,
				      points[1].x, points[1].y);
			gdk_draw_line(window, style->white_gc, points[2].x, points[2].y,
				      points[1].x, points[1].y);
		}
	else
		{
			switch (shadow_type)
				{
				case GTK_SHADOW_IN:
				case GTK_SHADOW_ETCHED_IN:
					gc1 = style->white_gc;
					gc2 = style->dark_gc[state_type];
					break;
					
				case GTK_SHADOW_OUT:
				case GTK_SHADOW_ETCHED_OUT:
					gc1 = style->black_gc;
					gc2 = style->light_gc[state_type];
					break;
				}
			
			if (area)
				{
					gdk_gc_set_clip_rectangle(gc1, area);
					gdk_gc_set_clip_rectangle(gc2, area);
				}
			
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
				}
			/* fill with background color */
			gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
					   x, y, width, height);
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
				}
			
			as = (width < height) ? (width - 2) / 3 : (height - 2) / 3;
			if ((arrow_type == GTK_ARROW_UP) || (arrow_type == GTK_ARROW_DOWN))
				{
					xl = x + width / 2 - as / 2 - 1;
					xm = xl + as - 1;
					xr = xm + as - 1;
					yt = y + as + 1;
					yb = yt + as - 1;
				}
			else
				{
					xl = x + as + 1;
					xr = xl + as - 1;
					yt = y + height / 2 - as / 2 - 1;
					ym = yt + as - 1;
					yb = ym + as - 1;
				}
			
			switch (arrow_type)
				{
				case GTK_ARROW_UP:
					gdk_draw_line(window, gc1, xl, yb, xm, yt);
					gdk_draw_line(window, gc1, xl+1, yb, xm, yt+1);
					
					gdk_draw_line(window, gc1, xr, yb, xm, yt);
					gdk_draw_line(window, gc1, xr-1, yb, xm, yt+1);
#ifdef BROKEN_MACH64
					gdk_draw_point(window, gc1, xm, yt);
#endif
					break;
					
				case GTK_ARROW_DOWN:
					gdk_draw_line(window, gc1, xl,   yt, xm, yb);
					gdk_draw_line(window, gc1, xl+1, yt, xm, yb-1);
					
					gdk_draw_line(window, gc1, xr,   yt, xm, yb);
					gdk_draw_line(window, gc1, xr-1, yt, xm, yb-1);
#ifdef BROKEN_MACH64
					gdk_draw_point(window, gc1, xm, yb);
#endif
					break;
					
				case GTK_ARROW_LEFT:
					gdk_draw_line(window, gc1, xr, yt,   xl,   ym);
					gdk_draw_line(window, gc1, xr, yt+1, xl+1, ym);
					
					gdk_draw_line(window, gc1, xr, yb,   xl,   ym);
					gdk_draw_line(window, gc1, xr, yb-1, xl+1, ym);
#ifdef BROKEN_MACH64
					gdk_draw_point(window, style->bg_gc[state_type], xl, ym-1);
					gdk_draw_point(window, style->bg_gc[state_type], xl, ym+1);
					gdk_draw_point(window, gc1, xl, ym);
#endif
					break;
					
				case GTK_ARROW_RIGHT:
					gdk_draw_line(window, gc1, xl, yt,   xr,   ym);
					gdk_draw_line(window, gc1, xl, yt+1, xr-1, ym);
					
					gdk_draw_line(window, gc1, xl, yb,   xr,   ym);
					gdk_draw_line(window, gc1, xl, yb-1, xr-1, ym);
#ifdef BROKEN_MACH64
					gdk_draw_point(window, style->bg_gc[state_type], xr, ym-1);
					gdk_draw_point(window, style->bg_gc[state_type], xr, ym+1);
					gdk_draw_point(window, gc1, xr, ym);
#endif
					break;
				}
			
			if (DETAIL("spinbutton") ||
			    !henzai_rc_style->flat_arrows ||
			    ((shadow_type == GTK_SHADOW_OUT) ||
			     (shadow_type == GTK_SHADOW_ETCHED_OUT)) &&
			    (detail && !strcmp(&detail[1], "scrollbar")))
				gtk_paint_shadow(style, window, state_type, shadow_type, area,
						 widget, detail, x, y, width, height);
			
			if (area)
				{
					gdk_gc_set_clip_rectangle(gc1, NULL);
					gdk_gc_set_clip_rectangle(gc2, NULL);
				}
		}
}

static void
draw_diamond(GtkStyle * style,
	     GdkWindow * window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle * area,
	     GtkWidget * widget,
	     const gchar *detail, gint x, gint y, gint width, gint height)
{
	gint half_width;
	gint half_height;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_diamond", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	half_width = width / 2;
	half_height = height / 2;
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->light_gc[state_type], area);
			gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
			gdk_gc_set_clip_rectangle(style->dark_gc[state_type], area);
			gdk_gc_set_clip_rectangle(style->black_gc, area);
		}
	switch (shadow_type)
		{
		case GTK_SHADOW_IN:
			gdk_draw_line(window, style->light_gc[state_type],
				      x + 2, y + half_height,
				      x + half_width, y + height - 2);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y + height - 2,
				      x + width - 2, y + half_height);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + 1, y + half_height,
				      x + half_width, y + height - 1);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y + height - 1,
				      x + width - 1, y + half_height);
			gdk_draw_line(window, style->light_gc[state_type],
				      x, y + half_height, x + half_width, y + height);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y + height,
				      x + width, y + half_height);
			
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + 2, y + half_height, x + half_width, y + 2);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y + 2,
				      x + width - 2, y + half_height);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + 1, y + half_height, x + half_width, y + 1);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y + 1,
				      x + width - 1, y + half_height);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x, y + half_height, x + half_width, y);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y, x + width, y + half_height);
			break;
		case GTK_SHADOW_OUT:
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + 2, y + half_height,
				      x + half_width, y + height - 2);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y + height - 2,
				      x + width - 2, y + half_height);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + 1, y + half_height,
				      x + half_width, y + height - 1);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y + height - 1,
				      x + width - 1, y + half_height);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x, y + half_height, x + half_width, y + height);
			gdk_draw_line(window, style->dark_gc[state_type],
				      x + half_width, y + height,
				      x + width, y + half_height);
			
			gdk_draw_line(window, style->light_gc[state_type],
				      x + 2, y + half_height, x + half_width, y + 2);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y + 2,
				      x + width - 2, y + half_height);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + 1, y + half_height, x + half_width, y + 1);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y + 1,
				      x + width - 1, y + half_height);
			gdk_draw_line(window, style->light_gc[state_type],
				      x, y + half_height, x + half_width, y);
			gdk_draw_line(window, style->light_gc[state_type],
				      x + half_width, y, x + width, y + half_height);
			break;
		default:
			break;
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->light_gc[state_type], NULL);
			gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
			gdk_gc_set_clip_rectangle(style->dark_gc[state_type], NULL);
			gdk_gc_set_clip_rectangle(style->black_gc, NULL);
		}
}

static void
draw_oval(GtkStyle * style,
	  GdkWindow * window,
	  GtkStateType state_type,
	  GtkShadowType shadow_type,
	  GdkRectangle * area,
	  GtkWidget * widget,
	  const gchar *detail, gint x, gint y, gint width, gint height)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
}

static void
draw_string(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    GdkRectangle * area,
	    GtkWidget * widget,
	    const gchar *detail, gint x, gint y, const gchar * string)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
	
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_string", detail);
#endif
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->white_gc, area);
			gdk_gc_set_clip_rectangle(style->fg_gc[state_type], area);
		}
	if (state_type == GTK_STATE_INSENSITIVE)
		gdk_draw_string(window, style->font, style->white_gc, x + 1, y + 1,
				string);
	gdk_draw_string(window, style->font, style->fg_gc[state_type], x, y,
			string);
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->white_gc, NULL);
			gdk_gc_set_clip_rectangle(style->fg_gc[state_type], NULL);
		}
}


/* utility function to draw a slider */
static void
draw_slider_pattern (GtkStyle          *style,
		     GdkWindow         *window,
		     gint              x,
		     gint              y,
		     gint              width,
		     gint              height,
		     GtkOrientation    orientation)
{
	int i;

	if (orientation == GTK_ORIENTATION_VERTICAL)
		for (i = 0; i < height/2; i++)
			{
				gdk_draw_line (window, style->black_gc, x,   y+(i*2),   x + width, y + (i*2)  );
				gdk_draw_line (window, style->white_gc, x,   y+(i*2)+1, x + width, y + (i*2)+1);
			}
	else
		for (i = 0; i < height/2; i++)
			{
				gdk_draw_line (window, style->black_gc, x+(i*2),   y, x+(i*2),   y + height);
				gdk_draw_line (window, style->white_gc, x+(i*2)+1, y, x+(i*2)+1, y + height);
			}
}


static void
draw_box(GtkStyle * style,
	 GdkWindow * window,
	 GtkStateType state_type,
	 GtkShadowType shadow_type,
	 GdkRectangle * area,
	 GtkWidget * widget,
	 const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkGC *light_gc;
	GdkGC *dark_gc;
	GtkOrientation orientation;
	HenzaiRcStyle *henzai_rc_style;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_box", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	light_gc = style->light_gc[state_type];
	dark_gc = style->dark_gc[state_type];
	
	orientation = (height > width) ? GTK_ORIENTATION_VERTICAL :
		GTK_ORIENTATION_HORIZONTAL;
	
	if (DETAIL("slider"))
		{
			int i;
			
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
					gdk_gc_set_clip_rectangle(style->light_gc[state_type], area);
					gdk_gc_set_clip_rectangle(style->dark_gc[state_type], area);
					gdk_gc_set_clip_rectangle(style->bg_gc[GTK_STATE_ACTIVE], area);
					gdk_gc_set_clip_rectangle(style->white_gc, area);
					gdk_gc_set_clip_rectangle(style->black_gc, area);
				}
#if 0			
			/* Just a plain rectangle with shadow ... */
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
							   x, y, width, height);
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}

			gtk_paint_shadow(style, window, state_type, shadow_type, area,
					 widget, detail, x, y, width, height);
			
			/* Draw dimple in the scrollbar handle */
			draw_dimple(style, window, state_type, width, height, x, y,
				    (orientation == GTK_ORIENTATION_HORIZONTAL));
#endif		

			draw_slider_pattern (style, window, x, y, width, height, orientation);

			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
					gdk_gc_set_clip_rectangle(style->light_gc[state_type], NULL);
					gdk_gc_set_clip_rectangle(style->dark_gc[state_type], NULL);
					gdk_gc_set_clip_rectangle(style->bg_gc[GTK_STATE_ACTIVE], NULL);
				}
		}
	else if (DETAIL("buttondefault"))
		{
			/* I don't want no background on default buttons..
			 * Let's add that cute triangle (see below) instead... */
		}
	else if (DETAIL("button"))
		{
			GdkPoint points1[3];	/* dark */
			GdkPoint points2[3];	/* light */
			
			points1[0].x = x + 2;
			points1[0].y = y + 2;
			points1[1].x = x + 8;
			points1[1].y = y + 2;
			points1[2].x = x + 2;
			points1[2].y = y + 8;
			points2[0].x = x + 3;
			points2[0].y = y + 3;
			points2[1].x = x + 8;
			points2[1].y = y + 3;
			points2[2].x = x + 3;
			points2[2].y = y + 8;
			
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
				}
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					gdk_draw_rectangle(window, style->bg_gc[state_type],
							   TRUE, x + 1, y + 1, width - 2, height - 2);
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			/* Paint a triangle here instead of in "buttondefault"
			 * which is drawn _behind_ the current button */
			if (GTK_WIDGET_HAS_DEFAULT(widget) && state_type != GTK_STATE_ACTIVE)
				{
					gdk_draw_polygon(window, style->black_gc, FALSE, points1, 3);
					gdk_draw_polygon(window, style->white_gc, FALSE, points2, 3);
					gdk_draw_polygon(window, style->bg_gc[GTK_STATE_ACTIVE],
							 TRUE, points2, 3);
				}
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
				}
			gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
					 detail, x, y, width, height);
		}
	else if (henzai_rc_style->flat_progress && DETAIL("bar"))
		{
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[GTK_STATE_SELECTED], area);
				}
			gdk_draw_rectangle(window, style->bg_gc[GTK_STATE_SELECTED],
					   TRUE, x - 1, y - 1, width + 2, height + 2);
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[GTK_STATE_SELECTED], NULL);
				}
		}
	else if (DETAIL("checkbutton"))
		{
			gint cx, cy;
			
			cx = x + width / 2 - 1;
			cy = y + height - 4;
			
			gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
					   x, y, width, height);
			gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
					 detail, x, y, width, height);
			if (shadow_type == GTK_SHADOW_IN)
				{
					gdk_draw_line(window, style->black_gc, cx, cy, cx - 1, cy - 1);
					gdk_draw_line(window, style->black_gc, cx, cy - 1, cx - 1, cy - 2);
					
					gdk_draw_line(window, style->black_gc, cx, cy, cx + 2, cy - 2);
					gdk_draw_line(window, style->black_gc, cx, cy - 1, cx + 2, cy - 3);
				}
		}
	else if (DETAIL("trough"))
		{
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
						}
					gdk_draw_rectangle(window, style->bg_gc[GTK_STATE_ACTIVE], TRUE,
							   x, y, width, height);
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
						}
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
					 detail, x, y, width, height);
		}
	else if (DETAIL("handlebox_bin"))
		{
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
						}
					gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
							   x, y, width, height);
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
						}
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			/*
			 * gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
			 * detail, x, y, width, height);
			 */
		}
	else if (DETAIL("menubar"))
		{
			
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
						}
					gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
							   x, y, width, height);
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
						}
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			
			/*
			 * gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
			 * detail, x, y, width, height);
			 */
		}
	else if (DETAIL("menuitem"))
		{
			
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
						}
					gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
							   x + 1, y, width - 2, height);
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
						}
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			
			gtk_paint_shadow(style, window, state_type, GTK_SHADOW_IN, area, widget,
					 detail, x + 1, y, width - 2, height);
			/*
			 * } else if (DETAIL("notebook")) {
			 * henzai_notebook(style, window, state_type, shadow_type, area, widget,
			 * detail, x, y, width, height);
			 */
		}
	else
		{
			if ((!style->bg_pixmap[state_type]) ||
			    GDK_IS_PIXMAP(window))
				{
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
						}
					gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
							   x, y, width, height);
					if (area)
						{
							gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
						}
				}
			else
				{
					gtk_style_apply_default_background(style, window,
									   widget &&
									   !GTK_WIDGET_NO_WINDOW(widget),
									   state_type, area, x, y, width,
									   height);
				}
			gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
					 detail, x, y, width, height);
		}
}



static void
draw_flat_box(GtkStyle * style,
	      GdkWindow * window,
	      GtkStateType state_type,
	      GtkShadowType shadow_type,
	      GdkRectangle * area,
	      GtkWidget * widget,
	      const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_flat_box", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	gc1 = style->bg_gc[state_type];
	
	if (DETAIL("text") && (state_type == GTK_STATE_SELECTED))
		gc1 = style->bg_gc[GTK_STATE_SELECTED];
	else if (DETAIL("viewportbin"))
		gc1 = style->bg_gc[GTK_STATE_NORMAL];
	else if (DETAIL("entry_bg"))
		gc1 = style->bg_gc[GTK_STATE_ACTIVE];
	if ((!style->bg_pixmap[state_type]) || (gc1 != style->bg_gc[state_type]) ||
	    GDK_IS_PIXMAP(window))
		{
			if (area)
				gdk_gc_set_clip_rectangle(gc1, area);
			
			gdk_draw_rectangle(window, gc1, TRUE, x, y, width, height);
			if (DETAIL("tooltip"))
				gdk_draw_rectangle(window, style->black_gc, FALSE,
						   x, y, width - 1, height - 1);
			if (area)
				gdk_gc_set_clip_rectangle(gc1, NULL);
		}
	else
		gtk_style_apply_default_background(style, window,
						   widget &&
						   !GTK_WIDGET_NO_WINDOW(widget),
						   state_type, area, x, y, width,
						   height);
}

static void
draw_check(GtkStyle * style,
	   GdkWindow * window,
	   GtkStateType state_type,
	   GtkShadowType shadow_type,
	   GdkRectangle * area,
	   GtkWidget * widget,
	   const gchar *detail, gint x, gint y, gint width, gint height)
{
	gint cx, cy;
	
	cx = x + width / 2 - 1;
	cy = y + (height / 2) + 1;
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_check", detail);
#endif
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->black_gc, area);
			gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
		}
	
	if (shadow_type == GTK_SHADOW_IN)
		{
			gdk_draw_rectangle(window, style->bg_gc[GTK_STATE_ACTIVE], TRUE,
					   x, y, width, height);
			
			gdk_draw_line(window, style->black_gc, cx, cy, cx - 1, cy - 1);
			gdk_draw_line(window, style->black_gc, cx, cy - 1, cx - 1, cy - 2);
			
			gdk_draw_line(window, style->black_gc, cx, cy, cx + 2, cy - 2);
			gdk_draw_line(window, style->black_gc, cx, cy - 1, cx + 2, cy - 3);
		}
	else
		{
			gdk_draw_rectangle(window, style->bg_gc[GTK_STATE_NORMAL], TRUE,
					   x, y, width, height);
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->black_gc, NULL);
			gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
		}
	gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
			 detail, x, y, width, height);
}


static void
draw_option(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle * area,
	    GtkWidget * widget,
	    const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1;
	GdkGC *gc2;
	GdkGC *gc3;
	HenzaiRcStyle *henzai_rc_style;

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_option", detail);
#endif
	if (shadow_type == GTK_SHADOW_IN)
		{
			SET_GC_IN(henzai_rc_style);
			gc3 = style->bg_gc[GTK_STATE_ACTIVE];
		}
	else
		{
			SET_GC_OUT(henzai_rc_style);
			gc3 = style->bg_gc[GTK_STATE_NORMAL];
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
			gdk_gc_set_clip_rectangle(gc3, area);
		}
	
	gdk_draw_arc(window, gc3, TRUE, x, y, width, height, 0, 360 * 64);
	gdk_draw_arc(window, gc1, FALSE, x, y, width, height, 45 * 64, 225 * 64);
	gdk_draw_arc(window, gc2, FALSE, x, y, width, height, 225 * 64, 180 * 64);
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
			gdk_gc_set_clip_rectangle(gc3, NULL);
		}
}


static void
draw_cross(GtkStyle * style,
	   GdkWindow * window,
	   GtkStateType state_type,
	   GtkShadowType shadow_type,
	   GdkRectangle * area,
	   GtkWidget * widget,
	   const gchar *detail, gint x, gint y, gint width, gint height)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
}

static void
draw_ramp(GtkStyle * style,
	  GdkWindow * window,
	  GtkStateType state_type,
	  GtkShadowType shadow_type,
	  GdkRectangle * area,
	  GtkWidget * widget,
	  const gchar *detail,
	  GtkArrowType arrow_type, gint x, gint y, gint width, gint height)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
}

static void
draw_tab(GtkStyle * style,
	 GdkWindow * window,
	 GtkStateType state_type,
	 GtkShadowType shadow_type,
	 GdkRectangle * area,
	 GtkWidget * widget,
	 const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkPoint points[3];
	GdkGC *gc1;
	GdkGC *gc2;
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_tab", detail);
#endif
	
	if (DETAIL("optionmenutab"))
		{
			/* draw little down arrow */
			points[0].x = x + 4;
			points[0].y = y + 2;
			points[1].x = points[0].x + (height - 4);
			points[1].y = points[0].y + (height - 4);
			points[2].x = points[0].x + (height - 4) * 2;
			points[2].y = y + 2;
			
			SET_GC_OUT(henzai_rc_style);
			gdk_draw_polygon(window, style->bg_gc[GTK_STATE_ACTIVE], TRUE, points,
					 3);
			gdk_draw_line(window, gc2, points[0].x, points[0].y,
				      points[2].x, points[2].y);
			gdk_draw_line(window, gc2, points[0].x, points[0].y,
				      points[1].x, points[1].y);
			gdk_draw_line(window, gc1, points[2].x, points[2].y,
				      points[1].x, points[1].y);
		}
	else
		gtk_paint_box (style, window, state_type, shadow_type, area, widget, detail,
			       x, y, width, height);
}

static void
draw_shadow_gap(GtkStyle * style,
		GdkWindow * window,
		GtkStateType state_type,
		GtkShadowType shadow_type,
		GdkRectangle * area,
		GtkWidget * widget,
		const gchar *detail,
		gint x,
		gint y,
		gint width,
		gint height,
		GtkPositionType gap_side, gint gap_x, gint gap_width)
{
	GdkRectangle rect;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_shadow_gap", detail);
#endif
	
	gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
			 detail, x, y, width, height);
	
	switch (gap_side)
		{
		case GTK_POS_TOP:
			rect.x = x + gap_x;
			rect.y = y;
			rect.width = gap_width;
			rect.height = 2;
			break;
		case GTK_POS_BOTTOM:
			rect.x = x + gap_x;
			rect.y = y + height - 2;
			rect.width = gap_width;
			rect.height = 2;
			break;
		case GTK_POS_LEFT:
			rect.x = x;
			rect.y = y + gap_x;
			rect.width = 2;
			rect.height = gap_width;
			break;
		case GTK_POS_RIGHT:
			rect.x = x + width - 2;
			rect.y = y + gap_x;
			rect.width = 2;
			rect.height = gap_width;
			break;
		}
	
	gtk_style_apply_default_pixmap(style, window, state_type, area,
				       rect.x, rect.y, rect.width, rect.height);
}

static void
draw_box_gap(GtkStyle * style,
	     GdkWindow * window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle * area,
	     GtkWidget * widget,
	     const gchar *detail,
	     gint x,
	     gint y,
	     gint width,
	     gint height, GtkPositionType gap_side, gint gap_x, gint gap_width)
{
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);

	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);
	
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_box_gap", detail);
#endif
	
	
	gtk_style_apply_default_background(style, window,
					   widget && !GTK_WIDGET_NO_WINDOW (widget),
					   state_type, area, x, y, width, height);
	
	if (width == -1 && height == -1)
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);
	
	switch (shadow_type)
		{
		case GTK_SHADOW_NONE:
			return;
		case GTK_SHADOW_IN:
		case GTK_SHADOW_ETCHED_IN:
			SET_GC_IN(henzai_rc_style);
			break;
		case GTK_SHADOW_OUT:
		case GTK_SHADOW_ETCHED_OUT:
			SET_GC_OUT(henzai_rc_style);
			break;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
		}
	
	switch (gap_side)
		{
		case GTK_POS_TOP:
			gdk_draw_line (window, gc1, x, y, x, y + height - 1);
			
			gdk_draw_line (window, gc2, x, y + height - 1, x + width - 1, y + height - 1);
			gdk_draw_line (window, gc2, x + width - 1, y, x + width - 1, y + height - 1);
			if (gap_x > 0)
				{
					gdk_draw_line (window, gc1, x, y, x + gap_x, y);
				}
			if ((width - (gap_x + gap_width)) > 0)
				{
					gdk_draw_line (window, gc1, x + gap_x + gap_width - 1, y, x + width - 2, y);
				}
			break;
			
		case  GTK_POS_BOTTOM:
			gdk_draw_line (window, gc1, x, y, x + width - 1, y);
			gdk_draw_line (window, gc1, x, y, x, y + height - 1);
			
			gdk_draw_line (window, gc2, x + width - 1, y, x + width - 1, y + height - 1);
			if (gap_x > 0)
				{
					gdk_draw_line (window, gc2, x, y + height - 1, x + gap_x - 2, y + height - 1);
				}
			if ((width - (gap_x + gap_width)) > 0)
				{
					gdk_draw_line (window, gc2, x + gap_x + gap_width, y + height - 1, x + width - 2, y + height - 1);
				}
			break;
			
		case GTK_POS_LEFT:
			gdk_draw_line (window, gc1, x, y, x + width - 1, y);
			
			gdk_draw_line (window, gc2, x, y + height - 1, x + width - 1, y + height - 1);
			gdk_draw_line (window, gc2, x + width - 1, y, x + width - 1, y + height - 1);
			if (gap_x > 0)
				{
					gdk_draw_line (window, gc1, x, y, x, y + gap_x);
				}
			if ((width - (gap_x + gap_width)) > 0)
				{
					gdk_draw_line (window, gc1, x, y + gap_x + gap_width - 1, x, y + height - 2);
				}
			break;
			
		case GTK_POS_RIGHT:
			gdk_draw_line (window, gc1, x, y, x + width - 1, y);
			gdk_draw_line (window, gc1, x, y, x, y + height - 1);
			
			gdk_draw_line (window, gc2, x, y + height - 1, x + width - 1, y + height - 1);
			if (gap_x > 0)
				{
					gdk_draw_line (window, gc2, x + width - 1, y, x + width - 1, y + gap_x);
				}
			if ((width - (gap_x + gap_width)) > 0)
				{
					gdk_draw_line (window, gc2, x + width - 1, y + gap_x + gap_width - 1, x + width - 1, y + height - 2);
				}
			break;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
		}
}

/*
 * draw notebook tabs
 */
static void
draw_extension(GtkStyle * style,
	       GdkWindow * window,
	       GtkStateType state_type,
	       GtkShadowType shadow_type,
	       GdkRectangle * area,
	       GtkWidget * widget,
	       const gchar *detail,
	       gint x,
	       gint y, gint width, gint height, GtkPositionType gap_side)
{
	GdkGC *gc1;
	GdkGC *gc2;
	HenzaiRcStyle *henzai_rc_style;

	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
	
	henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);

#ifdef DEBUG
	printf("%-17s %-15s %d %d %d x:%d y:%d wi:%d he:%d\n", "draw_extension",
	       detail, gap_side,
	       style->xthickness, style->ythickness,
	       x, y, width, height);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	switch (shadow_type)
		{
		case GTK_SHADOW_NONE:
			return;
		case GTK_SHADOW_IN:
		case GTK_SHADOW_ETCHED_IN:
			SET_GC_IN(henzai_rc_style);
			break;
		case GTK_SHADOW_OUT:
		case GTK_SHADOW_ETCHED_OUT:
			SET_GC_OUT(henzai_rc_style);
			break;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, area);
			gdk_gc_set_clip_rectangle(gc2, area);
		}
	
	
	gtk_style_apply_default_background(style, window,
					   widget && !GTK_WIDGET_NO_WINDOW(widget),
					   state_type, area, x, y,
					   width, height);
	
	switch (gap_side)
		{
		case GTK_POS_TOP:
			gtk_style_apply_default_pixmap(style, window, state_type, area,
						       x, y, width - 2, height - 1);
			gdk_draw_line(window, gc1, x, y, x, y+height);
			gdk_draw_line(window, gc2, x, y+height-1, x+width-1, y+height-1);
			gdk_draw_line(window, gc2, x+width-1, y+height-1, x+width-1, y-1);
			break;
			
		case GTK_POS_BOTTOM:
			gtk_style_apply_default_pixmap(style, window, state_type, area,
						       x+1, y+1, width-2, height-1);
			gdk_draw_line(window, gc1, x, y, x + width - 2, y);
			gdk_draw_line(window, gc1, x, y, x, y + height - 1);
			gdk_draw_line(window, gc2, x + width - 1, y, x + width - 1, y + height - 1);
			break;
			
		case GTK_POS_LEFT:
			gtk_style_apply_default_pixmap(style, window, state_type, area,
						       x, y + 1, width - 1, height - 2);
			gdk_draw_line(window, gc1, x, y, x + width, y);
			gdk_draw_line(window, gc2, x, y + height - 1, x + width, y+height-1);
			gdk_draw_line(window, gc2, x + width-1, y+1, x+width-1, y+height-1);
			break;
			
		case GTK_POS_RIGHT:
			gtk_style_apply_default_pixmap(style, window, state_type, area,
						       x+1, y+1, width-1, height-2);
			gdk_draw_line(window, gc1, x, y, x + width-1, y);
			gdk_draw_line(window, gc1, x, y+1, x, y+height-1);
			gdk_draw_line(window, gc2, x, y+height-1, x+width-1, y+height-1);
			break;
		}
	
	if (area)
		{
			gdk_gc_set_clip_rectangle(gc1, NULL);
			gdk_gc_set_clip_rectangle(gc2, NULL);
		}
}

static void
draw_focus(GtkStyle * style,
	   GdkWindow * window,
	   GdkRectangle * area,
	   GtkWidget * widget,
	   const gchar *detail, gint x, gint y, gint width, gint height)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
	return;
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_focus", detail);
#endif
	
	if ((width == -1) && (height == -1))
		{
			gdk_window_get_size(window, &width, &height);
			width -= 1;
			height -= 1;
		}
	else if (width == -1)
		{
			gdk_window_get_size(window, &width, NULL);
			width -= 1;
		}
	else if (height == -1)
		{
			gdk_window_get_size(window, NULL, &height);
			height -= 1;
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->black_gc, area);
		}
	
	//    draw_shadow(style, window, GTK_STATE_NORMAL, GTK_SHADOW_OUT, NULL,
	//      widget, NULL, x, y, width, height);
	
	gdk_draw_rectangle(window, style->black_gc, FALSE, x, y, width, height);
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->black_gc, NULL);
		}
}

static void
draw_slider(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle * area,
	    GtkWidget * widget,
	    const gchar *detail,
	    gint x, gint y, gint width, gint height, GtkOrientation orientation)
{
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_slider", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	gtk_draw_box(style, window, state_type, shadow_type, x, y, width, height);
	if (orientation == GTK_ORIENTATION_HORIZONTAL)
		draw_vline(style, window, state_type, area, widget, detail,
			   style->ythickness,
			   height - style->ythickness - 1, width / 2);
	else
		draw_hline(style, window, state_type, area, widget, detail,
			   style->xthickness,
			   width - style->xthickness - 1, height / 2);
}

static void
draw_handle(GtkStyle * style,
	    GdkWindow * window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle * area,
	    GtkWidget * widget,
	    const gchar *detail,
	    gint x, gint y, gint width, gint height, GtkOrientation orientation)
{
	GdkGC *light_gc, *dark_gc;
	GdkRectangle dest;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
#ifdef DEBUG
	printf("%-17s %-15s\n", "draw_handle", detail);
#endif
	
	if ((width == -1) && (height == -1))
		gdk_window_get_size(window, &width, &height);
	else if (width == -1)
		gdk_window_get_size(window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size(window, NULL, &height);
	
	gtk_paint_box(style, window, state_type, shadow_type, area, widget,
		      detail, x, y, width, height);
	
	//    draw_dimple(style, window, state_type, width, height, x, y, TRUE);
	if (height > width)
		draw_vline(style, window, state_type, area, widget, detail,
			   height/2-2, height/2+2, width/2-1);
	else
		draw_hline(style, window, state_type, area, widget, detail,
			   width/2-1, width/2+1, height/2-1);
}



static void
henzai_tab(GtkStyle * style,
	  GdkWindow * window,
	  GtkStateType state_type,
	  GtkShadowType shadow_type,
	  GdkRectangle * area,
	  GtkWidget * widget,
	  const gchar *detail, gint x, gint y, gint width, gint height)
{
	GtkNotebook *notebook;
	GdkGC *lightgc, *darkgc;
	int orientation;
	
#ifdef DEBUG
	printf("%-17s %-15s\n", "henzai_tab", detail);
#endif
	
	
	notebook = GTK_NOTEBOOK(widget);
	orientation = notebook->tab_pos;
	
	lightgc = style->light_gc[state_type];
	darkgc = style->dark_gc[state_type];
	
	if ((!style->bg_pixmap[state_type]) ||
	    GDK_IS_PIXMAP(window))
		{
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], area);
				}
			gdk_draw_rectangle(window, style->bg_gc[state_type], TRUE,
					   x, y, width, height);
			if (area)
				{
					gdk_gc_set_clip_rectangle(style->bg_gc[state_type], NULL);
				}
		}
	else
		{
			gtk_style_apply_default_background(style, window,
							   widget &&
							   !GTK_WIDGET_NO_WINDOW(widget),
							   state_type, area, x, y, width,
							   height);
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->dark_gc[state_type], area);
			gdk_gc_set_clip_rectangle(style->light_gc[state_type], area);
		}
	switch (orientation)
		{
		case GTK_POS_TOP:
			gdk_draw_line(window, lightgc, x, y + height - 1, x, y);
			gdk_draw_line(window, lightgc, x, y, x + width - 1, y);
			gdk_draw_line(window, darkgc,
				      x + width - 1, y, x + width - 1, y + height - 1);
			break;
		case GTK_POS_BOTTOM:
			gdk_draw_line(window, lightgc, x, y, x, y + height - 1);
			gdk_draw_line(window, darkgc,
				      x, y + height - 1, x + width - 1, y + height - 1);
			gdk_draw_line(window, darkgc,
				      x + width - 1, y + height - 1, x + width - 1, y);
			break;
		case GTK_POS_LEFT:
			gdk_draw_line(window, lightgc, x, y + height - 1, x, y);
			gdk_draw_line(window, lightgc, x, y, x + width - 1, y);
			gdk_draw_line(window, darkgc,
				      x, y + height - 1, x + width - 1, y + height - 1);
			break;
		case GTK_POS_RIGHT:
			gdk_draw_line(window, lightgc, x, y, x + width - 1, y);
			gdk_draw_line(window, darkgc,
				      x + width - 1, y, x + width - 1, y + height - 1);
			gdk_draw_line(window, darkgc,
				      x, y + height - 1, x + width - 1, y + height - 1);
			break;
		}
	if (area)
		{
			gdk_gc_set_clip_rectangle(style->dark_gc[state_type], NULL);
			gdk_gc_set_clip_rectangle(style->light_gc[state_type], NULL);
		}
	gtk_paint_shadow(style, window, state_type, shadow_type, area, widget,
			 detail, x, y, width, height);
}




/****** other drawing functions implementation ******/



static void
henzai_set_background (GtkStyle * style, GdkWindow * window, GtkStateType state_type)
{
	GdkPixmap *pixmap;
	gint parent_relative;
	
	g_return_if_fail(style != NULL);
	g_return_if_fail(window != NULL);
	
	if (style->bg_pixmap[state_type])
		{
			if (style->bg_pixmap[state_type] == (GdkPixmap *) GDK_PARENT_RELATIVE)
				{
					pixmap = NULL;
					parent_relative = TRUE;
				}
			else
				{
					pixmap = style->bg_pixmap[state_type];
					parent_relative = FALSE;
				}
			
			gdk_window_set_back_pixmap(window, pixmap, parent_relative);
		}
	else
		gdk_window_set_background(window, &style->bg[state_type]);
}




/****** Theme administrative functions ******/



static void
hls_to_rgb(gdouble * h, gdouble * l, gdouble * s)
{
    gdouble hue;
    gdouble lightness;
    gdouble saturation;
    gdouble m1, m2;
    gdouble r, g, b;

    lightness = *l;
    saturation = *s;

    if (lightness <= 0.5)
	m2 = lightness * (1 + saturation);
    else
	m2 = lightness + saturation - lightness * saturation;
    m1 = 2 * lightness - m2;

    if (saturation == 0)
    {
	*h = lightness;
	*l = lightness;
	*s = lightness;
    }
    else
    {
	hue = *h + 120;
	while (hue > 360)
	    hue -= 360;
	while (hue < 0)
	    hue += 360;

	if (hue < 60)
	    r = m1 + (m2 - m1) * hue / 60;
	else if (hue < 180)
	    r = m2;
	else if (hue < 240)
	    r = m1 + (m2 - m1) * (240 - hue) / 60;
	else
	    r = m1;

	hue = *h;
	while (hue > 360)
	    hue -= 360;
	while (hue < 0)
	    hue += 360;

	if (hue < 60)
	    g = m1 + (m2 - m1) * hue / 60;
	else if (hue < 180)
	    g = m2;
	else if (hue < 240)
	    g = m1 + (m2 - m1) * (240 - hue) / 60;
	else
	    g = m1;

	hue = *h - 120;
	while (hue > 360)
	    hue -= 360;
	while (hue < 0)
	    hue += 360;

	if (hue < 60)
	    b = m1 + (m2 - m1) * hue / 60;
	else if (hue < 180)
	    b = m2;
	else if (hue < 240)
	    b = m1 + (m2 - m1) * (240 - hue) / 60;
	else
	    b = m1;

	*h = r;
	*l = g;
	*s = b;
    }
}

static void
rgb_to_hls(gdouble * r, gdouble * g, gdouble * b)
{
    gdouble min;
    gdouble max;
    gdouble red;
    gdouble green;
    gdouble blue;
    gdouble h, l, s;
    gdouble delta;

    red = *r;
    green = *g;
    blue = *b;

    if (red > green)
    {
	if (red > blue)
	    max = red;
	else
	    max = blue;

	if (green < blue)
	    min = green;
	else
	    min = blue;
    }
    else
    {
	if (green > blue)
	    max = green;
	else
	    max = blue;

	if (red < blue)
	    min = red;
	else
	    min = blue;
    }

    l = (max + min) / 2;
    s = 0;
    h = 0;

    if (max != min)
    {
	if (l <= 0.5)
	    s = (max - min) / (max + min);
	else
	    s = (max - min) / (2 - max - min);

	delta = max - min;
	if (red == max)
	    h = (green - blue) / delta;
	else if (green == max)
	    h = 2 + (blue - red) / delta;
	else if (blue == max)
	    h = 4 + (red - green) / delta;

	h *= 60;
	if (h < 0.0)
	    h += 360;
    }

    *r = h;
    *g = l;
    *b = s;
}


static void
henzai_style_shade(GdkColor * a, GdkColor * b, gdouble k)
{
    gdouble red;
    gdouble green;
    gdouble blue;

    red = (gdouble) a->red / 65535.0;
    green = (gdouble) a->green / 65535.0;
    blue = (gdouble) a->blue / 65535.0;

    rgb_to_hls(&red, &green, &blue);

    green *= k;
    if (green > 1.0)
	green = 1.0;
    else if (green < 0.0)
	green = 0.0;

    blue *= k;
    if (blue > 1.0)
	blue = 1.0;
    else if (blue < 0.0)
	blue = 0.0;

    hls_to_rgb(&red, &green, &blue);

    b->red = red * 65535.0;
    b->green = green * 65535.0;
    b->blue = blue * 65535.0;
}





static void
henzai_realize_style (GtkStyle * style)
{
	HenzaiRcStyle *henzai_rc_style = HENZAI_RC_STYLE (style->rc_style);
	GdkGCValuesMask gc_values_mask = GDK_GC_FOREGROUND | GDK_GC_FONT;
	GdkGCValues gc_values;
	gint i;

	parent_class->realize (style);
#if 0
	/* this might need to be replaced with a Pango stuff */
	extern GdkFont *default_font;
	
	if (!default_font)
		default_font
			=
			gdk_font_load
			("-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*");
	
	if (style->font->type == GDK_FONT_FONT)
		gc_values.font = style->font;
	else if (style->font->type == GDK_FONT_FONTSET)
		gc_values.font = default_font;
	
#endif

	for (i = 0; i < 5; i++)
		{
			/*
			 *  Release old GC
			 */
			if (style->light_gc[i])
				gtk_gc_release(style->light_gc[i]);
			if (style->dark_gc[i])
				gtk_gc_release(style->dark_gc[i]);
			
			/*
			 *  Compute new colors
			 */
			henzai_style_shade(&style->bg[i], &style->light[i],
					  henzai_rc_style->lightness_multiplier);
			henzai_style_shade(&style->bg[i], &style->dark[i],
					  henzai_rc_style->darkness_multiplier);
			
			style->mid[i].red = (style->light[i].red + style->dark[i].red) / 2;
			style->mid[i].green =
				(style->light[i].green + style->dark[i].green) / 2;
			style->mid[i].blue = (style->light[i].blue + style->dark[i].blue) / 2;
			
			/*
			 *  Allocate new gc
			 */
			if (!gdk_color_alloc(style->colormap, &style->light[i]))
				g_warning("unable to allocate color: ( %d %d %d )",
					  style->light[i].red, style->light[i].green,
					  style->light[i].blue);
			if (!gdk_color_alloc(style->colormap, &style->dark[i]))
				g_warning("unable to allocate color: ( %d %d %d )",
					  style->dark[i].red, style->dark[i].green,
					  style->dark[i].blue);
			if (!gdk_color_alloc(style->colormap, &style->mid[i]))
				g_warning("unable to allocate color: ( %d %d %d )",
					  style->mid[i].red, style->mid[i].green,
					  style->mid[i].blue);
			
			gc_values.foreground = style->light[i];
			style->light_gc[i] = gtk_gc_get(style->depth, style->colormap,
							&gc_values, gc_values_mask);
			
			gc_values.foreground = style->dark[i];
			style->dark_gc[i] = gtk_gc_get(style->depth, style->colormap,
						       &gc_values, gc_values_mask);
			
			gc_values.foreground = style->mid[i];
			style->mid_gc[i] = gtk_gc_get(style->depth, style->colormap,
						      &gc_values, gc_values_mask);
		}
}




/****** Theme initialization/registration code *******/



static void
henzai_style_init (HenzaiStyle *style)
{
}

static void
henzai_style_class_init (HenzaiStyleClass *klass)
{
	GtkStyleClass *style_class = GTK_STYLE_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	style_class->draw_hline = draw_hline;
	style_class->draw_vline = draw_vline;
	style_class->draw_shadow = draw_shadow;
	style_class->draw_polygon = draw_polygon;
	style_class->draw_arrow = draw_arrow;
	style_class->draw_diamond = draw_diamond;
	style_class->draw_oval = draw_oval;
	style_class->draw_string = draw_string;
	style_class->draw_box = draw_box;
	style_class->draw_flat_box = draw_flat_box;
	style_class->draw_check = draw_check;
	style_class->draw_option = draw_option;
	style_class->draw_cross = draw_cross;
	style_class->draw_ramp = draw_ramp;
	style_class->draw_tab = draw_tab;
	style_class->draw_shadow_gap = draw_shadow_gap;
	style_class->draw_box_gap = draw_box_gap;
	style_class->draw_extension = draw_extension;
	style_class->draw_focus = draw_focus;
	style_class->draw_slider = draw_slider;
	style_class->draw_handle = draw_handle;
	
	style_class->set_background = henzai_set_background;
	style_class->realize = henzai_realize_style;
	
}

void
henzai_style_register_type (GtkThemeEngine *engine)
{
  static const GTypeInfo object_info =
  {
    sizeof (HenzaiStyleClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) henzai_style_class_init,
    NULL,           /* class_finalize */
    NULL,           /* class_data */
    sizeof (HenzaiStyle),
    0,              /* n_preallocs */
    (GInstanceInitFunc) henzai_style_init,
  };
  
  henzai_type_style = gtk_theme_engine_register_type (engine,
						     GTK_TYPE_STYLE,
						     "HenzaiStyle",
						     &object_info);
}
