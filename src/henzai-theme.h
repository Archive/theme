/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8; fill-column: 160 -*- */
/*
 * $Id$
 *
 * Copyright (C) 1999 Dwight Engen <dengen40@yahoo.com>
 *
 * The sources to the GTKstep theme by Ullrich Hafner <hafner@bigfoot.de>
 * and the Thin Ice theme by Tim & Tomas <timg@means.net> &
 * <stric@ing.umu.se> were both invaluable and borrowed from.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * $Log$
 * Revision 1.1  2000/09/07 11:42:08  bertrand
 * Initial revision
 *
 * Revision 0.1  1999/12/30 20:54:18  dengen
 * Initial
 *
 */

#include <gtk/gtk.h>
#include <gmodule.h>

/* Default stuff */
#define DEFAULT_BLACKANDWHITE		TRUE
#define DEFAULT_FLATARROWS		TRUE
#define DEFAULT_FLATPROGRESS		FALSE
#define DEFAULT_SMALLMENUARROWS		TRUE

#define DEFAULT_SLIDER_WIDTH      7
#define DEFAULT_STEPPER_WIDTH     7
#define DEFAULT_STEPPER_HEIGHT    20

#define DEFAULT_MIN_SLIDER_SIZE   18

enum
{
    TOKEN_BLACKANDWHITE = G_TOKEN_LAST +1,
    TOKEN_FLATPROGRESS,
    TOKEN_FLATARROWS,
    TOKEN_SMALLMENUARROWS,
    TOKEN_LIGHTNESSMULTIPLIER,
    TOKEN_DARKNESSMULTIPLIER,
    TOKEN_TRUE,
    TOKEN_FALSE
};





