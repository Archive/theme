/*
 * $Id$
 *
 * Copyright (C) 1999 Dwight Engen <dengen40@yahoo.com>
 *
 * The sources to the GTKstep theme by Ullrich Hafner <hafner@bigfoot.de>
 * and the Thin Ice theme by Tim & Tomas <timg@means.net> &
 * <stric@ing.umu.se> were both invaluable and borrowed from.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * $Log$
 * Revision 1.1  2000/09/07 11:42:08  bertrand
 * Initial revision
 *
 * Revision 0.2  2000/01/23 15:49:56  dengen
 * fixed horizontal scrollbar drawing problem (when it was empty)
 *
 * Revision 0.1  1999/12/30 20:54:18  dengen
 * Initial
 *
 */


#include "henzai-theme.h"
#include "henzai-theme-rc-style.h"
#include <gtk/gtk.h>
#include <gmodule.h>
#include <stdio.h>


#define SCROLL_DELAY_LENGTH  300
#define SCROLLBAR_SPACING(obj) (GTK_SCROLLED_WINDOW_GET_CLASS(obj)->scrollbar_spacing)
#define RANGE_CLASS(obj)  (GTK_RANGE_GET_CLASS(obj))


/* *INDENT-OFF* */
/* Theme functions to export */
void theme_init(GtkThemeEngine * engine);
void theme_exit(void);

/* Exported vtable from th_draw */
extern GtkStyleClass henzai_default_class;

/* Local prototypes */
static void henzai_range_remove_timer(GtkRange *range);
static void henzai_range_vslider_update(GtkRange *range);
static void henzai_range_hslider_update(GtkRange *range);
static gint henzai_range_vtrough_click(GtkRange *range, gint x, gint y, gfloat *jump_perc);
static gint henzai_range_htrough_click(GtkRange *range, gint x, gint y, gfloat *jump_perc);
static void henzai_range_vmotion(GtkRange *range, gint xdelta, gint ydelta);
static void henzai_range_hmotion(GtkRange *range, gint xdelta, gint ydelta);
static void henzai_range_trough_vdims(GtkRange *range, gint *top,  gint *bottom);
static void henzai_range_trough_hdims(GtkRange *range, gint *left, gint *right);
static void henzai_vscrollbar_calc_slider_size(GtkVScrollbar *vscrollbar);
static void henzai_vscrollbar_slider_update(GtkRange *range);
static void henzai_vscrollbar_realize (GtkWidget *widget);
static void henzai_vscrollbar_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void henzai_hscrollbar_calc_slider_size(GtkHScrollbar *hscrollbar);
static void henzai_hscrollbar_slider_update(GtkRange *range);
static void henzai_hscrollbar_realize (GtkWidget *widget);
static void henzai_hscrollbar_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static void henzai_scrolled_window_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static void henzai_scrolled_window_relative_allocation (GtkWidget *widget, GtkAllocation *allocation);













G_MODULE_EXPORT void
theme_init (GtkThemeEngine * engine)
{
    GtkRangeClass *rangeclass;
    GtkHScrollbarClass *hscrollbarclass;
    GtkVScrollbarClass *vscrollbarclass;
    GtkWidgetClass *widgetclass;

    henzai_rc_style_register_type (engine);
    henzai_style_register_type (engine);

    fprintf (stderr, "in theme initialization\n");
    /*
     * This code was ad[oa]pted from GTKstep theme. It moves both scrollbar
     * buttons of a horizontal scrollbar to the right and of a vertical
     * scrollbar to the bottom.
     */
    hscrollbarclass =
	(GtkHScrollbarClass *) gtk_type_class(gtk_hscrollbar_get_type());
    rangeclass = (GtkRangeClass *) hscrollbarclass;
    widgetclass = (GtkWidgetClass *) hscrollbarclass;

    rangeclass->slider_width = DEFAULT_SLIDER_WIDTH;
    rangeclass->min_slider_size = DEFAULT_MIN_SLIDER_SIZE;
    rangeclass->stepper_size = DEFAULT_STEPPER_WIDTH;
    rangeclass->stepper_height = DEFAULT_STEPPER_HEIGHT;
    rangeclass->stepper_slider_spacing = 0;

    rangeclass->motion = henzai_range_hmotion;
    rangeclass->trough_click = henzai_range_htrough_click;
    rangeclass->slider_update = henzai_hscrollbar_slider_update;
    widgetclass->realize = henzai_hscrollbar_realize;
    widgetclass->size_allocate = henzai_hscrollbar_size_allocate;

    vscrollbarclass =
	(GtkVScrollbarClass *) gtk_type_class(gtk_vscrollbar_get_type());
    rangeclass = (GtkRangeClass *) vscrollbarclass;
    widgetclass = (GtkWidgetClass *) vscrollbarclass;

    rangeclass->slider_width = DEFAULT_SLIDER_WIDTH;
    rangeclass->min_slider_size = DEFAULT_MIN_SLIDER_SIZE;
    rangeclass->stepper_size = DEFAULT_STEPPER_WIDTH;
    rangeclass->stepper_height = DEFAULT_STEPPER_HEIGHT;
    rangeclass->stepper_slider_spacing = 0;

    rangeclass->motion = henzai_range_vmotion;
    rangeclass->trough_click = henzai_range_vtrough_click;
    rangeclass->slider_update = henzai_vscrollbar_slider_update;
    widgetclass->realize = henzai_vscrollbar_realize;
    widgetclass->size_allocate = henzai_vscrollbar_size_allocate;
    printf ("Theme initialise\n");
}

G_MODULE_EXPORT void
theme_exit (void)
{
}


G_MODULE_EXPORT GtkRcStyle *
theme_create_rc_style (void)
{
  printf ("Creating an instance of HenzaiRcStyle\n");
  return GTK_RC_STYLE (g_object_new (HENZAI_TYPE_RC_STYLE, NULL));  
}



G_MODULE_EXPORT const gchar *
g_module_check_init (GModule * module)
{
    return gtk_check_version(GTK_MAJOR_VERSION,
			     GTK_MINOR_VERSION,
			     GTK_MICRO_VERSION - GTK_INTERFACE_AGE);
}



















/*
 * Following is the scrollbar code
 */


static void
henzai_hscrollbar_realize (GtkWidget * widget)
{
    GtkRange *range;
    GdkWindowAttr attributes;
    gint attributes_mask;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_HSCROLLBAR(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
    range = GTK_RANGE(widget);

    attributes.x = widget->allocation.x;
    attributes.y =
	widget->allocation.y + (widget->allocation.height -
				widget->requisition.height) / 2;
    attributes.width = widget->allocation.width;
    attributes.height = widget->requisition.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.visual = gtk_widget_get_visual(widget);
    attributes.colormap = gtk_widget_get_colormap(widget);
    attributes.event_mask = gtk_widget_get_events(widget);
    attributes.event_mask |= (GDK_EXPOSURE_MASK |
			      GDK_BUTTON_PRESS_MASK |
			      GDK_BUTTON_RELEASE_MASK |
			      GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
    widget->window =
	gdk_window_new(gtk_widget_get_parent_window(widget), &attributes,
		       attributes_mask);

    range->trough = widget->window;
    gdk_window_ref(range->trough);


    /* back arrow */
    attributes.x = widget->allocation.width -
	widget->style->xthickness -
	RANGE_CLASS(widget)->stepper_height * 2;
    attributes.y = 0;
    attributes.width = RANGE_CLASS(widget)->stepper_height;
    attributes.height = RANGE_CLASS(widget)->stepper_size;
    range->step_back =
	gdk_window_new(range->trough, &attributes, attributes_mask);

    /* forward arrow */
    attributes.x = widget->allocation.width -
	widget->style->xthickness - RANGE_CLASS(widget)->stepper_height;
    range->step_forw =
	gdk_window_new(range->trough, &attributes, attributes_mask);



    /* slider */
    attributes.x = 0;
    attributes.y = widget->style->ythickness;
    attributes.width = RANGE_CLASS(widget)->min_slider_size;
    attributes.height = RANGE_CLASS(widget)->slider_width;
    attributes.event_mask |= (GDK_BUTTON_MOTION_MASK |
			      GDK_POINTER_MOTION_HINT_MASK);
    range->slider = gdk_window_new(range->trough, &attributes, attributes_mask);

    henzai_hscrollbar_calc_slider_size(GTK_HSCROLLBAR(widget));
    gtk_range_slider_update(GTK_RANGE(widget));

    widget->style = gtk_style_attach(widget->style, widget->window);

    gdk_window_set_user_data(range->trough, widget);
    gdk_window_set_user_data(range->slider, widget);
    gdk_window_set_user_data(range->step_forw, widget);
    gdk_window_set_user_data(range->step_back, widget);

    gtk_style_set_background(widget->style, range->trough, GTK_STATE_ACTIVE);
    gtk_style_set_background(widget->style, range->slider, GTK_STATE_NORMAL);
    gtk_style_set_background(widget->style, range->step_forw, GTK_STATE_ACTIVE);
    gtk_style_set_background(widget->style, range->step_back, GTK_STATE_ACTIVE);

    gdk_window_show(range->slider);
    gdk_window_show(range->step_back);
    gdk_window_show(range->step_forw);
}

static void
henzai_vscrollbar_realize(GtkWidget * widget)
{
    GtkRange *range;
    GdkWindowAttr attributes;
    gint attributes_mask;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VSCROLLBAR(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
    range = GTK_RANGE(widget);

    attributes.x =
	widget->allocation.x + (widget->allocation.width -
				widget->requisition.width) / 2;
    attributes.y = widget->allocation.y;
    attributes.width = widget->requisition.width;
    attributes.height = widget->allocation.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.visual = gtk_widget_get_visual(widget);
    attributes.colormap = gtk_widget_get_colormap(widget);
    attributes.event_mask = gtk_widget_get_events(widget);
    attributes.event_mask |= (GDK_EXPOSURE_MASK |
			      GDK_BUTTON_PRESS_MASK |
			      GDK_BUTTON_RELEASE_MASK |
			      GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
    widget->window =
	gdk_window_new(gtk_widget_get_parent_window(widget), &attributes,
		       attributes_mask);

    range->trough = widget->window;
    gdk_window_ref(range->trough);

    attributes.x = 0;
    attributes.y = (widget->allocation.height -
		    widget->style->ythickness -
		    2 * RANGE_CLASS(widget)->stepper_height);
    attributes.width = RANGE_CLASS(widget)->stepper_size;
    attributes.height = RANGE_CLASS(widget)->stepper_height;

    range->step_back =
	gdk_window_new(range->trough, &attributes, attributes_mask);

    attributes.y = (widget->allocation.height -
		    widget->style->ythickness -
		    RANGE_CLASS(widget)->stepper_height);

    range->step_forw =
	gdk_window_new(range->trough, &attributes, attributes_mask);

    attributes.x = widget->style->ythickness;
    attributes.y = 0;
    attributes.width = RANGE_CLASS(widget)->slider_width;
    attributes.height = RANGE_CLASS(widget)->min_slider_size;
    attributes.event_mask |= (GDK_BUTTON_MOTION_MASK |
			      GDK_POINTER_MOTION_HINT_MASK);

    range->slider = gdk_window_new(range->trough, &attributes, attributes_mask);

    henzai_vscrollbar_calc_slider_size(GTK_VSCROLLBAR(widget));
    gtk_range_slider_update(GTK_RANGE(widget));

    widget->style = gtk_style_attach(widget->style, widget->window);

    gdk_window_set_user_data(range->trough, widget);
    gdk_window_set_user_data(range->slider, widget);
    gdk_window_set_user_data(range->step_forw, widget);
    gdk_window_set_user_data(range->step_back, widget);

    gtk_style_set_background(widget->style, range->trough, GTK_STATE_ACTIVE);
    gtk_style_set_background(widget->style, range->slider, GTK_STATE_NORMAL);
    gtk_style_set_background(widget->style, range->step_forw, GTK_STATE_ACTIVE);
    gtk_style_set_background(widget->style, range->step_back, GTK_STATE_ACTIVE);

    gdk_window_show(range->slider);
    gdk_window_show(range->step_back);
    gdk_window_show(range->step_forw);
}

static void
henzai_hscrollbar_size_allocate(GtkWidget * widget, GtkAllocation * allocation)
{
    GtkRange *range;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_HSCROLLBAR(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget))
    {
	range = GTK_RANGE(widget);

	gdk_window_move_resize(range->trough,
			       allocation->x,
			       allocation->y + (allocation->height -
						widget->requisition.height) /
			       2, allocation->width,
			       widget->requisition.height);
	gdk_window_move_resize(range->step_back,
			       widget->allocation.width -
			       widget->style->xthickness -
			       RANGE_CLASS(widget)->stepper_height * 2,
			       0,
			       RANGE_CLASS(widget)->stepper_height,
			       widget->requisition.height);

	gdk_window_move_resize(range->step_forw,
			       widget->allocation.width -
			       widget->style->xthickness -
			       RANGE_CLASS(widget)->stepper_height,
			       0,
			       RANGE_CLASS(widget)->stepper_height,
			       widget->requisition.height);

	gdk_window_resize(range->slider,
			  RANGE_CLASS(widget)->min_slider_size,
			  widget->requisition.height -
			  widget->style->ythickness * 2);

	gtk_range_slider_update(GTK_RANGE(widget));
    }
}

static void
henzai_vscrollbar_size_allocate(GtkWidget * widget, GtkAllocation * allocation)
{
    GtkRange *range;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VSCROLLBAR(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget))
    {
	range = GTK_RANGE(widget);

	gdk_window_move_resize(range->trough,
			       allocation->x + (allocation->width -
						widget->requisition.width) /
			       2, allocation->y, widget->requisition.width,
			       allocation->height);
	gdk_window_move_resize(range->step_back,
			       0,
			       allocation->height -
			       widget->style->ythickness -
			       2 * RANGE_CLASS(widget)->stepper_height,
			       widget->requisition.width,
			       RANGE_CLASS(widget)->stepper_height);
	gdk_window_move_resize(range->step_forw,
			       0,
			       allocation->height -
			       widget->style->ythickness -
			       RANGE_CLASS(widget)->stepper_height,
			       widget->requisition.width ,
			       RANGE_CLASS(widget)->stepper_height);
	gdk_window_resize(range->slider,
			  widget->requisition.width -
			  widget->style->xthickness * 2,
			  RANGE_CLASS(range)->min_slider_size);

	gtk_range_slider_update(GTK_RANGE(widget));
    }
}

static void
henzai_hscrollbar_slider_update(GtkRange * range)
{
    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_HSCROLLBAR(range));

    henzai_hscrollbar_calc_slider_size(GTK_HSCROLLBAR(range));
    henzai_range_hslider_update(range);
}

static void
henzai_vscrollbar_slider_update(GtkRange * range)
{
    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_VSCROLLBAR(range));

    henzai_vscrollbar_calc_slider_size(GTK_VSCROLLBAR(range));
    henzai_range_vslider_update(range);
}

static void
henzai_range_trough_hdims(GtkRange * range, gint * left, gint * right)
{
    gint trough_width;
    gint slider_length;
    gint tmp_width;
    gint tleft;
    gint tright;

    g_return_if_fail(range != NULL);

    gdk_window_get_size(range->trough, &trough_width, NULL);
    gdk_window_get_size(range->slider, &slider_length, NULL);

    tleft = GTK_WIDGET(range)->style->xthickness;
    tright =
	trough_width - 1 - slider_length -
	GTK_WIDGET(range)->style->xthickness;




    if (range->step_back)
    {
	gdk_window_get_size(range->step_back, &tmp_width, NULL);
	tright -= (tmp_width + RANGE_CLASS(range)->stepper_slider_spacing);
    }

    if (range->step_forw)
    {
	gdk_window_get_size(range->step_forw, &tmp_width, NULL);
	tright -= (tmp_width + RANGE_CLASS(range)->stepper_slider_spacing);
    }

    if (left)
	*left = tleft;
    if (right)
	*right = tright;
}

static void
henzai_range_trough_vdims(GtkRange * range, gint * top, gint * bottom)
{
    gint trough_height;
    gint slider_length;
    gint tmp_height;
    gint ttop;
    gint tbottom;

    g_return_if_fail(range != NULL);

    gdk_window_get_size(range->trough, NULL, &trough_height);
    gdk_window_get_size(range->slider, NULL, &slider_length);

    ttop = GTK_WIDGET(range)->style->ythickness;
    tbottom =
	trough_height - 1 - slider_length -
	GTK_WIDGET(range)->style->ythickness;

    if (range->step_back)
    {
	gdk_window_get_size(range->step_back, NULL, &tmp_height);
	tbottom -= (tmp_height + RANGE_CLASS(range)->stepper_slider_spacing);
    }

    if (range->step_forw)
    {
	gdk_window_get_size(range->step_forw, NULL, &tmp_height);
	tbottom -= (tmp_height + RANGE_CLASS(range)->stepper_slider_spacing);
    }

    if (top)
	*top = ttop;
    if (bottom)
	*bottom = tbottom;
}

static void
henzai_range_hmotion(GtkRange * range, gint xdelta, gint ydelta)
{
    gdouble old_value;
    gint left, right;
    gint slider_x, slider_y;
    gint new_pos;

    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_RANGE(range));

    range = GTK_RANGE(range);

    gdk_window_get_position(range->slider, &slider_x, &slider_y);
    henzai_range_trough_hdims(range, &left, &right);

    if (left == right)
	return;

    new_pos = slider_x + xdelta;

    if (new_pos < left)
	new_pos = left;
    else if (new_pos > right)
	new_pos = right;

    old_value = range->adjustment->value;
    range->adjustment->value = ((range->adjustment->upper -
				 range->adjustment->lower -
				 range->adjustment->page_size) *
				(new_pos - left) / (right - left) +
				range->adjustment->lower);

    if (range->digits >= 0)
    {
	char buffer[64];

	sprintf(buffer, "%0.*f", range->digits, range->adjustment->value);
	sscanf(buffer, "%f", &range->adjustment->value);
    }

    if (old_value != range->adjustment->value)
    {
	if (range->policy == GTK_UPDATE_CONTINUOUS)
	{
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}
	else
	{
	    gtk_range_slider_update(range);
	    gtk_range_clear_background(range);

	    if (range->policy == GTK_UPDATE_DELAYED)
	    {
		henzai_range_remove_timer(range);
		range->timer = gtk_timeout_add(SCROLL_DELAY_LENGTH,
					       (GtkFunction)
					       RANGE_CLASS(range)->timer,
					       (gpointer) range);
	    }
	}
    }
}

static void
henzai_range_vmotion(GtkRange * range, gint xdelta, gint ydelta)
{
    gdouble old_value;
    gint top, bottom;
    gint slider_x, slider_y;
    gint new_pos;

    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_RANGE(range));

    range = GTK_RANGE(range);

    gdk_window_get_position(range->slider, &slider_x, &slider_y);
    henzai_range_trough_vdims(range, &top, &bottom);

    if (bottom == top)
	return;

    new_pos = slider_y + ydelta;

    if (new_pos < top)
	new_pos = top;
    else if (new_pos > bottom)
	new_pos = bottom;

    old_value = range->adjustment->value;
    range->adjustment->value = ((range->adjustment->upper -
				 range->adjustment->lower -
				 range->adjustment->page_size) *
				(new_pos - top) / (bottom - top) +
				range->adjustment->lower);

    if (range->digits >= 0)
    {
	char buffer[64];

	sprintf(buffer, "%0.*f", range->digits, range->adjustment->value);
	sscanf(buffer, "%f", &range->adjustment->value);
    }

    if (old_value != range->adjustment->value)
    {
	if (range->policy == GTK_UPDATE_CONTINUOUS)
	{
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}
	else
	{
	    gtk_range_slider_update(range);
	    gtk_range_clear_background(range);

	    if (range->policy == GTK_UPDATE_DELAYED)
	    {
		henzai_range_remove_timer(range);
		range->timer = gtk_timeout_add(SCROLL_DELAY_LENGTH,
					       (GtkFunction)
					       RANGE_CLASS(range)->timer,
					       (gpointer) range);
	    }
	}
    }
}

static gint
henzai_range_htrough_click(GtkRange * range, gint x, gint y, gfloat * jump_perc)
{
    gint ythickness;
    gint trough_width;
    gint trough_height;
    gint slider_x;
    gint slider_length;
    gint left, right;

    g_return_val_if_fail(range != NULL, GTK_TROUGH_NONE);
    g_return_val_if_fail(GTK_IS_RANGE(range), GTK_TROUGH_NONE);

    ythickness = GTK_WIDGET(range)->style->ythickness;

    henzai_range_trough_hdims(range, &left, &right);
    gdk_window_get_size(range->slider, &slider_length, NULL);
    right += slider_length;

    if ((x > left) && (y > ythickness))
    {
	gdk_window_get_size(range->trough, &trough_width, &trough_height);

	if ((x < right) && (y < (trough_height - ythickness)))
	{
	    if (jump_perc)
	    {
		*jump_perc =
		    ((gdouble) (x - left)) / ((gdouble) (right - left));
		return GTK_TROUGH_JUMP;
	    }

	    gdk_window_get_position(range->slider, &slider_x, NULL);

	    if (x < slider_x)
		return GTK_TROUGH_START;
	    else
		return GTK_TROUGH_END;
	}
    }

    return GTK_TROUGH_NONE;
}

static gint
henzai_range_vtrough_click(GtkRange * range, gint x, gint y, gfloat * jump_perc)
{
    gint xthickness;
    gint trough_width;
    gint trough_height;
    gint slider_y;
    gint top, bottom;
    gint slider_length;

    g_return_val_if_fail(range != NULL, GTK_TROUGH_NONE);
    g_return_val_if_fail(GTK_IS_RANGE(range), GTK_TROUGH_NONE);

    xthickness = GTK_WIDGET(range)->style->xthickness;

    henzai_range_trough_vdims(range, &top, &bottom);
    gdk_window_get_size(range->slider, NULL, &slider_length);
    bottom += slider_length;

    if ((x > xthickness) && (y > top))
    {
	gdk_window_get_size(range->trough, &trough_width, &trough_height);

	if ((x < (trough_width - xthickness) && (y < bottom)))
	{
	    if (jump_perc)
	    {
		*jump_perc = ((gdouble) (y - top)) / ((gdouble) (bottom - top));

		return GTK_TROUGH_JUMP;
	    }

	    gdk_window_get_position(range->slider, NULL, &slider_y);

	    if (y < slider_y)
		return GTK_TROUGH_START;
	    else
		return GTK_TROUGH_END;
	}
    }

    return GTK_TROUGH_NONE;
}

static void
henzai_range_hslider_update(GtkRange * range)
{
    gint left;
    gint right;
    gint x;

    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_RANGE(range));

    if (GTK_WIDGET_REALIZED(range))
    {
	henzai_range_trough_hdims(range, &left, &right);
	x = left;

	if (range->adjustment->value < range->adjustment->lower)
	{
	    range->adjustment->value = range->adjustment->lower;
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}
	else if (range->adjustment->value > range->adjustment->upper)
	{
	    range->adjustment->value = range->adjustment->upper;
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}

	if (range->adjustment->lower !=
	    (range->adjustment->upper - range->adjustment->page_size))
	    x +=
		((right - left) *
		 (range->adjustment->value -
		  range->adjustment->lower) / (range->adjustment->upper -
					       range->adjustment->lower -
					       range->adjustment->page_size));

	if (x < left)
	    x = left;
	else if (x > right)
	    x = right;

	gdk_window_move(range->slider, x,
			GTK_WIDGET(range)->style->ythickness);
    }
}

static void
henzai_range_vslider_update(GtkRange * range)
{
    gint top;
    gint bottom;
    gint y;

    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_RANGE(range));

    if (GTK_WIDGET_REALIZED(range))
    {
	henzai_range_trough_vdims(range, &top, &bottom);
	y = top;

	if (range->adjustment->value < range->adjustment->lower)
	{
	    range->adjustment->value = range->adjustment->lower;
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}
	else if (range->adjustment->value > range->adjustment->upper)
	{
	    range->adjustment->value = range->adjustment->upper;
	    gtk_signal_emit_by_name(GTK_OBJECT(range->adjustment),
				    "value_changed");
	}

	if (range->adjustment->lower !=
	    (range->adjustment->upper - range->adjustment->page_size))
	{
	    y +=
		((bottom - top) *
		 (range->adjustment->value -
		  range->adjustment->lower) / (range->adjustment->upper -
					       range->adjustment->lower -
					       range->adjustment->page_size));
	}

	if (y < top)
	    y = top;
	else if (y > bottom)
	    y = bottom;

	gdk_window_move(range->slider,
			GTK_WIDGET(range)->style->xthickness, y);
    }
}

static void
henzai_hscrollbar_calc_slider_size(GtkHScrollbar * hscrollbar)
{
    GtkRange *range;
    gint step_back_x;
    gint step_back_width;
    gint step_forw_x;
    gint step_forw_width;
    gint trough_width;
    gint slider_width;
    gint slider_height;
    gint left, right;
    gint width;

    g_return_if_fail(hscrollbar != NULL);
    g_return_if_fail(GTK_IS_HSCROLLBAR(hscrollbar));

    if (GTK_WIDGET_REALIZED(hscrollbar))
    {
	range = GTK_RANGE(hscrollbar);

	gdk_window_get_size(range->step_back, &step_back_width, NULL);
	gdk_window_get_size(range->step_forw, &step_forw_width, NULL);
	gdk_window_get_size(range->trough, &trough_width, NULL);
	gdk_window_get_position(range->step_back, &step_back_x, NULL);
	gdk_window_get_position(range->step_forw, &step_forw_x, NULL);

	left = step_back_width + step_forw_width +
		RANGE_CLASS (hscrollbar)->stepper_slider_spacing +
		GTK_WIDGET(hscrollbar)->style->xthickness;
	right = GTK_WIDGET(hscrollbar)->allocation.width
	    - 1 - GTK_WIDGET(hscrollbar)->style->xthickness;
	width = right - left;

	if ((range->adjustment->page_size > 0) &&
	    (range->adjustment->lower != range->adjustment->upper))
	{
	    if (range->adjustment->page_size >
		(range->adjustment->upper - range->adjustment->lower))
		range->adjustment->page_size =
		    range->adjustment->upper - range->adjustment->lower;

	    width = (width * range->adjustment->page_size /
		     (range->adjustment->upper - range->adjustment->lower));

	    if (width < RANGE_CLASS(hscrollbar)->min_slider_size)
		width = RANGE_CLASS(hscrollbar)->min_slider_size;
	}

	gdk_window_get_size(range->slider, &slider_width, &slider_height);

	if (slider_width != width)
	    gdk_window_resize(range->slider, width, slider_height);
    }
}

static void
henzai_vscrollbar_calc_slider_size(GtkVScrollbar * vscrollbar)
{
    GtkRange *range;
    gint step_back_y;
    gint step_back_height;
    gint step_forw_y;
    gint step_forw_width;
    gint trough_width;
    gint slider_width;
    gint slider_height;
    gint top, bottom;
    gint height;

    g_return_if_fail(vscrollbar != NULL);
    g_return_if_fail(GTK_IS_VSCROLLBAR(vscrollbar));

    if (GTK_WIDGET_REALIZED(vscrollbar))
    {
	range = GTK_RANGE(vscrollbar);

	gdk_window_get_size(range->step_back, NULL, &step_back_height);
	gdk_window_get_size(range->step_forw, &step_forw_width, NULL);
	gdk_window_get_size(range->trough, &trough_width, NULL);
	gdk_window_get_position(range->step_back, NULL, &step_back_y);
	gdk_window_get_position(range->step_forw, NULL, &step_forw_y);

	top = GTK_WIDGET(vscrollbar)->style->ythickness;
	bottom = step_back_y -
	    RANGE_CLASS(vscrollbar)->stepper_slider_spacing * 2 - 1;
	height = bottom - top;

	if ((range->adjustment->page_size > 0) &&
	    (range->adjustment->lower != range->adjustment->upper))
	{
	    if (range->adjustment->page_size >
		(range->adjustment->upper - range->adjustment->lower))
		range->adjustment->page_size =
		    range->adjustment->upper - range->adjustment->lower;

	    height = (height * range->adjustment->page_size /
		      (range->adjustment->upper - range->adjustment->lower));

	    if (height < RANGE_CLASS(vscrollbar)->min_slider_size)
		height = RANGE_CLASS(vscrollbar)->min_slider_size;
	}

	gdk_window_get_size(range->slider, &slider_width, &slider_height);

	if (slider_height != height)
	    gdk_window_resize(range->slider, slider_width, height);
    }
}



static void
henzai_scrolled_window_relative_allocation(GtkWidget * widget,
					  GtkAllocation * allocation)
{
    GtkScrolledWindow *scrolled_window;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(allocation != NULL);

    scrolled_window = GTK_SCROLLED_WINDOW(widget);

    allocation->x = GTK_CONTAINER(widget)->border_width;
    allocation->y = GTK_CONTAINER(widget)->border_width;
    allocation->width =
	MAX(1, (gint) widget->allocation.width - allocation->x * 2);
    allocation->height =
	MAX(1, (gint) widget->allocation.height - allocation->y * 2);

    if (scrolled_window->vscrollbar_visible)
    {
	GtkRequisition vscrollbar_requisition;

	gtk_widget_get_child_requisition(scrolled_window->vscrollbar,
					 &vscrollbar_requisition);

	if (scrolled_window->window_placement == GTK_CORNER_TOP_RIGHT ||
	    scrolled_window->window_placement == GTK_CORNER_BOTTOM_RIGHT)
	    allocation->x += (vscrollbar_requisition.width +
			      SCROLLBAR_SPACING(scrolled_window));

	allocation->width = MAX(1, (gint) allocation->width -
				((gint) vscrollbar_requisition.width +
				 (gint) SCROLLBAR_SPACING(scrolled_window)));
    }
    if (scrolled_window->hscrollbar_visible)
    {
	GtkRequisition hscrollbar_requisition;

	gtk_widget_get_child_requisition(scrolled_window->hscrollbar,
					 &hscrollbar_requisition);

	if (scrolled_window->window_placement == GTK_CORNER_BOTTOM_LEFT ||
	    scrolled_window->window_placement == GTK_CORNER_BOTTOM_RIGHT)
	    allocation->y += (hscrollbar_requisition.height +
			      SCROLLBAR_SPACING(scrolled_window));

	allocation->height = MAX(1, (gint) allocation->height -
				 ((gint) hscrollbar_requisition.height +
				  (gint) SCROLLBAR_SPACING(scrolled_window)));
    }
}

static void
henzai_scrolled_window_size_allocate(GtkWidget * widget,
				    GtkAllocation * allocation)
{
    GtkScrolledWindow *scrolled_window;
    GtkBin *bin;
    GtkAllocation relative_allocation;
    GtkAllocation child_allocation;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_SCROLLED_WINDOW(widget));
    g_return_if_fail(allocation != NULL);

    scrolled_window = GTK_SCROLLED_WINDOW(widget);
    bin = GTK_BIN(scrolled_window);

    widget->allocation = *allocation;

    if (scrolled_window->hscrollbar_policy == GTK_POLICY_ALWAYS)
	scrolled_window->hscrollbar_visible = TRUE;
    else if (scrolled_window->hscrollbar_policy == GTK_POLICY_NEVER)
	scrolled_window->hscrollbar_visible = FALSE;
    if (scrolled_window->vscrollbar_policy == GTK_POLICY_ALWAYS)
	scrolled_window->vscrollbar_visible = TRUE;
    else if (scrolled_window->vscrollbar_policy == GTK_POLICY_NEVER)
	scrolled_window->vscrollbar_visible = FALSE;

    if (bin->child && GTK_WIDGET_VISIBLE(bin->child))
    {
	gboolean previous_hvis;
	gboolean previous_vvis;
	guint count = 0;

	do
	{
	    henzai_scrolled_window_relative_allocation(widget,
						      &relative_allocation);

	    child_allocation.x = relative_allocation.x + allocation->x;
	    child_allocation.y = relative_allocation.y + allocation->y;
	    child_allocation.width = relative_allocation.width;
	    child_allocation.height = relative_allocation.height;

	    previous_hvis = scrolled_window->hscrollbar_visible;
	    previous_vvis = scrolled_window->vscrollbar_visible;

	    gtk_widget_size_allocate(bin->child, &child_allocation);

	    /* If, after the first iteration, the hscrollbar and the
	     * vscrollbar flip visiblity, then we need both.
	     */
	    if (count &&
		previous_hvis != scrolled_window->hscrollbar_visible &&
		previous_vvis != scrolled_window->vscrollbar_visible)
	    {
		scrolled_window->hscrollbar_visible = TRUE;
		scrolled_window->vscrollbar_visible = TRUE;

		/* a new resize is already queued at this point,
		 * so we will immediatedly get reinvoked
		 */
		return;
	    }

	    count++;
	}
	while (previous_hvis != scrolled_window->hscrollbar_visible ||
	       previous_vvis != scrolled_window->vscrollbar_visible);
    }
    else
	henzai_scrolled_window_relative_allocation(widget, &relative_allocation);

    if (scrolled_window->hscrollbar_visible)
    {
	GtkRequisition hscrollbar_requisition;

	gtk_widget_get_child_requisition(scrolled_window->hscrollbar,
					 &hscrollbar_requisition);

	if (!GTK_WIDGET_VISIBLE(scrolled_window->hscrollbar))
	    gtk_widget_show(scrolled_window->hscrollbar);

	child_allocation.x = relative_allocation.x;
	if (scrolled_window->window_placement == GTK_CORNER_TOP_LEFT ||
	    scrolled_window->window_placement == GTK_CORNER_TOP_RIGHT)
	    child_allocation.y = (relative_allocation.y +
				  relative_allocation.height +
				  SCROLLBAR_SPACING(scrolled_window));
	else
	    child_allocation.y = GTK_CONTAINER(scrolled_window)->border_width;

	child_allocation.width = relative_allocation.width;
	child_allocation.height = hscrollbar_requisition.height;
	child_allocation.x += allocation->x;
	child_allocation.y += allocation->y;

	gtk_widget_size_allocate(scrolled_window->hscrollbar,
				 &child_allocation);
    }
    else if (GTK_WIDGET_VISIBLE(scrolled_window->hscrollbar))
	gtk_widget_hide(scrolled_window->hscrollbar);

    if (scrolled_window->vscrollbar_visible)
    {
	GtkRequisition vscrollbar_requisition;

	if (!GTK_WIDGET_VISIBLE(scrolled_window->vscrollbar))
	    gtk_widget_show(scrolled_window->vscrollbar);

	gtk_widget_get_child_requisition(scrolled_window->vscrollbar,
					 &vscrollbar_requisition);

	if (scrolled_window->window_placement == GTK_CORNER_TOP_LEFT ||
	    scrolled_window->window_placement == GTK_CORNER_BOTTOM_LEFT)
	    child_allocation.x = (relative_allocation.x +
				  relative_allocation.width +
				  SCROLLBAR_SPACING(scrolled_window));
	else
	    child_allocation.x = GTK_CONTAINER(scrolled_window)->border_width;

	child_allocation.y = relative_allocation.y;
	child_allocation.width = vscrollbar_requisition.width;
	child_allocation.height = relative_allocation.height;
	child_allocation.x += allocation->x;
	child_allocation.y += allocation->y;
	if (scrolled_window->window_placement == GTK_CORNER_TOP_RIGHT
	    && scrolled_window->hscrollbar_visible)
	    child_allocation.height += SCROLLBAR_SPACING(scrolled_window)
		+ scrolled_window->hscrollbar->requisition.height;

	gtk_widget_size_allocate(scrolled_window->vscrollbar,
				 &child_allocation);
    }
    else if (GTK_WIDGET_VISIBLE(scrolled_window->vscrollbar))
	gtk_widget_hide(scrolled_window->vscrollbar);
}

static void
henzai_range_remove_timer(GtkRange * range)
{
    g_return_if_fail(range != NULL);
    g_return_if_fail(GTK_IS_RANGE(range));

    if (range->timer)
    {
	gtk_timeout_remove(range->timer);
	range->timer = 0;
    }
    range->need_timer = FALSE;
}
