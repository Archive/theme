/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8; fill-column: 160 -*- */

#include <gtk/gtkstyle.h>

typedef struct _HenzaiStyle HenzaiStyle;
typedef struct _HenzaiStyleClass HenzaiStyleClass;

extern GType henzai_type_style;

#define HENZAI_TYPE_STYLE              henzai_type_style
#define HENZAI_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HENZAI_TYPE_STYLE, HenzaiStyle))
#define HENZAI_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HENZAI_TYPE_STYLE, HenzaiStyleClass))
#define HENZAI_IS_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HENZAI_TYPE_STYLE))
#define HENZAI_IS_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HENZAI_TYPE_STYLE))
#define HENZAI_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HENZAI_TYPE_STYLE, HenzaiStyleClass))

struct _HenzaiStyle
{
  GtkStyle parent_instance;
};

struct _HenzaiStyleClass
{
  GtkStyleClass parent_class;
};

void henzai_style_register_type (GtkThemeEngine *engine);
