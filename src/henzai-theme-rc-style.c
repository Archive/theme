/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8; fill-column: 160 -*- */

#include "henzai-theme.h"
#include "henzai-theme-rc-style.h"
#include "henzai-theme-style.h"
#include <stdio.h>

static void      henzai_rc_style_init         (HenzaiRcStyle       *style);
static void      henzai_rc_style_class_init   (HenzaiRcStyleClass  *klass);
static void      henzai_rc_style_finalize     (GObject            *object);
static guint     henzai_rc_style_parse        (GtkRcStyle         *rc_style,
					      GScanner           *scanner);
static void      henzai_rc_style_merge        (GtkRcStyle         *dest,
					      GtkRcStyle         *src);
static GtkStyle *henzai_rc_style_create_style (GtkRcStyle         *rc_style);




/* Theme parsing */
static struct
{
	gchar *name;
	guint  token;
}
theme_symbols[] =

{
	{ "black_and_white",		TOKEN_BLACKANDWHITE },
	{ "flat_progress",		TOKEN_FLATPROGRESS },
	{ "flat_active_arrows",	        TOKEN_FLATARROWS },
	{ "small_menu_arrows",	        TOKEN_SMALLMENUARROWS },
	
	{ "lightness_multiplier",	TOKEN_LIGHTNESSMULTIPLIER },
	{ "darkness_multiplier",	TOKEN_DARKNESSMULTIPLIER },
	
	{ "TRUE",			TOKEN_TRUE },
	{ "FALSE",			TOKEN_FALSE },
};

static guint n_theme_symbols = sizeof(theme_symbols) / sizeof(theme_symbols[0]);


/* *INDENT-ON* */



static guint
henzai_parse_boolean(GScanner * scanner, HenzaiRcStyle *henzai_style)
{
	guint token, which_token;
	gboolean value;
	
	which_token = g_scanner_get_next_token(scanner);
	if ((which_token != TOKEN_BLACKANDWHITE) &&
	    (which_token != TOKEN_FLATARROWS) &&
	    (which_token != TOKEN_FLATPROGRESS) &&
	    (which_token != TOKEN_SMALLMENUARROWS))
		return TOKEN_BLACKANDWHITE;
	
	token = g_scanner_get_next_token(scanner);
	if (token != G_TOKEN_EQUAL_SIGN)
		return G_TOKEN_EQUAL_SIGN;
	
	token = g_scanner_get_next_token(scanner);
	if (token == TOKEN_TRUE)
		value = TRUE;
	else if (token == TOKEN_FALSE)
		value = FALSE;
	else
		return TOKEN_TRUE;
	
	switch(which_token)
		{
		case TOKEN_BLACKANDWHITE:
			henzai_style->black_and_white = value;
			break;
		case TOKEN_FLATARROWS:
			henzai_style->flat_arrows = value;
			break;
		case TOKEN_FLATPROGRESS:
			henzai_style->flat_progress = value;
			break;
		case TOKEN_SMALLMENUARROWS:
			henzai_style->small_menu_arrows = value;
			break;
		default:
			break;
		}
	return G_TOKEN_NONE;
}


static guint
henzai_parse_multiplier(GScanner * scanner, HenzaiRcStyle *henzai_style)
{
	guint token, which_token;
	
	which_token = g_scanner_get_next_token(scanner);
	if ((which_token != TOKEN_LIGHTNESSMULTIPLIER) &&
	    (which_token != TOKEN_DARKNESSMULTIPLIER))
		return TOKEN_LIGHTNESSMULTIPLIER;
	
	token = g_scanner_get_next_token(scanner);
	if (token != G_TOKEN_EQUAL_SIGN)
		return G_TOKEN_EQUAL_SIGN;
	
	token = g_scanner_get_next_token(scanner);
	if (token == G_TOKEN_FLOAT)
		{
			if (which_token == TOKEN_LIGHTNESSMULTIPLIER)
				henzai_style->lightness_multiplier = scanner->value.v_float;
			else
				henzai_style->darkness_multiplier = scanner->value.v_float;
		}
	else
		return G_TOKEN_FLOAT;
	
	return G_TOKEN_NONE;
}



guint
henzai_rc_style_parse (GtkRcStyle * rc_style, GScanner *scanner)
{
	HenzaiRcStyle *henzai_style = HENZAI_RC_STYLE (rc_style);
	static GQuark scope_id = 0;
	guint old_scope;
	guint token;
	guint i;
	
	/* Set up a new scope in this scanner. */

	if (!scope_id)
		scope_id = g_quark_from_string("henzai_theme_engine");
	
	/*
	 * If we bail out due to errors, we *don't* reset the scope, so the
	 * error messaging code can make sense of our tokens.
	 */
	old_scope = g_scanner_set_scope(scanner, scope_id);
	
	/*
	 * Now check if we already added our symbols to this scope
	 * (in some previous call to henzai_parse_rc_style for the
	 * same scanner.
	 */
	
	if (!g_scanner_lookup_symbol(scanner, theme_symbols[0].name))
		{
			g_scanner_freeze_symbol_table(scanner);
			for (i = 0; i < n_theme_symbols; i++)
				{
					g_scanner_scope_add_symbol(scanner, scope_id,
								   theme_symbols[i].name,
								   GINT_TO_POINTER(theme_symbols[i].token));
				}
			g_scanner_thaw_symbol_table(scanner);
		}
	
	/* We're ready to go, now parse the top level */
	
	henzai_style->black_and_white    = DEFAULT_BLACKANDWHITE;
	henzai_style->flat_arrows        = DEFAULT_FLATARROWS;
	henzai_style->flat_progress      = DEFAULT_FLATPROGRESS;
	henzai_style->small_menu_arrows  = DEFAULT_SMALLMENUARROWS;
	

	token = g_scanner_peek_next_token(scanner);
	
	while (token != G_TOKEN_RIGHT_CURLY)
		{
			switch (token)
				{
				case TOKEN_BLACKANDWHITE:
				case TOKEN_FLATARROWS:
				case TOKEN_FLATPROGRESS:
				case TOKEN_SMALLMENUARROWS:
					token = henzai_parse_boolean(scanner, henzai_style);
					break;
				case TOKEN_LIGHTNESSMULTIPLIER:
				case TOKEN_DARKNESSMULTIPLIER:
					token = henzai_parse_multiplier(scanner, henzai_style);
					break;
				default:
					g_scanner_get_next_token(scanner);
					token = G_TOKEN_RIGHT_CURLY;
					break;
				}


			if (token != G_TOKEN_NONE)
				{
					
					return token;
				}
			token = g_scanner_peek_next_token(scanner);
		}

	g_scanner_get_next_token(scanner);
	
	g_scanner_set_scope(scanner, old_scope);
	
	return G_TOKEN_NONE;
}




static void
henzai_merge_rc_style(GtkRcStyle * dest, GtkRcStyle * src)
{
	HenzaiRcStyle *dest_data, *src_data;

	dest_data = HENZAI_RC_STYLE (dest);
	src_data = HENZAI_RC_STYLE (src);
	
	dest_data->lightness_multiplier = src_data->lightness_multiplier;
	dest_data->darkness_multiplier = src_data->darkness_multiplier;
	dest_data->black_and_white = src_data->black_and_white;
	dest_data->flat_arrows = src_data->flat_arrows;
	dest_data->flat_progress = src_data->flat_progress;
	dest_data->small_menu_arrows = src_data->small_menu_arrows;
}





static GtkRcStyleClass *parent_class;

GType henzai_type_rc_style = 0;

void
henzai_rc_style_register_type (GtkThemeEngine *engine)
{
	static const GTypeInfo object_info =
	{
		sizeof (HenzaiRcStyleClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) henzai_rc_style_class_init,
		NULL,           /* class_finalize */
		NULL,           /* class_data */
		sizeof (HenzaiRcStyle),
		0,              /* n_preallocs */
		(GInstanceInitFunc) henzai_rc_style_init,
	};
	henzai_type_rc_style = gtk_theme_engine_register_type (engine,
							      GTK_TYPE_RC_STYLE,
							      "HenzaiRcStyle",
							      &object_info);
}

static void
henzai_rc_style_init (HenzaiRcStyle *style)
{
}

static void
henzai_rc_style_class_init (HenzaiRcStyleClass *klass)
{
	GtkRcStyleClass *rc_style_class = GTK_RC_STYLE_CLASS (klass);
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	parent_class = g_type_class_peek_parent (klass);
	
	rc_style_class->parse = henzai_rc_style_parse;
	rc_style_class->merge = henzai_rc_style_merge;
	rc_style_class->create_style = henzai_rc_style_create_style;
	
	object_class->finalize = henzai_rc_style_finalize;
}

static void
henzai_rc_style_finalize (GObject *object)
{
	HenzaiRcStyle *rc_style = HENZAI_RC_STYLE (object);
	
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
henzai_rc_style_merge (GtkRcStyle *dest,
		      GtkRcStyle *src)
{
	if (HENZAI_IS_RC_STYLE (src))
		{
			HenzaiRcStyle *henzai_dest = HENZAI_RC_STYLE (dest);
			HenzaiRcStyle *henzai_src = HENZAI_RC_STYLE (src);
			
			henzai_dest->black_and_white        = henzai_src->black_and_white;
			henzai_dest->flat_progress          = henzai_src->flat_progress;
			henzai_dest->flat_arrows            = henzai_src->flat_arrows;
			henzai_dest->small_menu_arrows      = henzai_src->small_menu_arrows;
			henzai_dest->lightness_multiplier   = henzai_src->lightness_multiplier;
			henzai_dest->darkness_multiplier    = henzai_src->darkness_multiplier;

		}
	
	parent_class->merge (dest, src);
}

/* Create an empty style suitable to this RC style
 */
static GtkStyle *
henzai_rc_style_create_style (GtkRcStyle *rc_style)
{
	return GTK_STYLE (g_object_new (HENZAI_TYPE_STYLE, NULL));
}



