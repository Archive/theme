/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8; fill-column: 160 -*- */

#include <gtk/gtkrc.h>

typedef struct _HenzaiRcStyle HenzaiRcStyle;
typedef struct _HenzaiRcStyleClass HenzaiRcStyleClass;

extern GType henzai_type_rc_style;

#define HENZAI_TYPE_RC_STYLE              henzai_type_rc_style
#define HENZAI_RC_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), HENZAI_TYPE_RC_STYLE, HenzaiRcStyle))
#define HENZAI_RC_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), HENZAI_TYPE_RC_STYLE, HenzaiRcStyleClass))
#define HENZAI_IS_RC_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), HENZAI_TYPE_RC_STYLE))
#define HENZAI_IS_RC_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), HENZAI_TYPE_RC_STYLE))
#define HENZAI_RC_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), HENZAI_TYPE_RC_STYLE, HenzaiRcStyleClass))

struct _HenzaiRcStyle
{
	GtkRcStyle parent_instance;
	
	gboolean	black_and_white;
	gboolean	flat_progress;
	gboolean	flat_arrows;
	gboolean	small_menu_arrows;
	gfloat	        lightness_multiplier;
	gfloat	        darkness_multiplier;
};

struct _HenzaiRcStyleClass
{
	GtkRcStyleClass parent_class;
};

void henzai_rc_style_register_type (GtkThemeEngine *engine);

